#ifndef RRT_RRTSMART_H_
#define RRT_RRTSMART_H_

#include "RRTFunctions.h"

class RRTSmart: public virtual RRTFunctions {
public:
	RRTSmart();
	virtual ~RRTSmart();
protected:
	Node* generateNodeForRRTSmart();
	bool useRRTSmartSampler();
	void initSmart();
private:
	void sampleNodeForRRTSmart();

	const unsigned int BIAS_RATIO = 7; // TODO laut Paper, testen ...
	// über wieviele Abschnitte minimal/maximal probiert werden soll eine Abkürzung zu finden
	const unsigned int MIN_STEPSIZE = 5;
	const unsigned int MAX_STEPSIZE = 100;

	int currentNumber;
	std::list<Position> positions;
	Node* parentNodeForSmart;
	double lastoptimizedCost;
	Node* lastOptimizedNode;
	bool lastfound;
	bool lastWasFlexible;
};

#endif /* RRT_RRTSMART_H_ */
