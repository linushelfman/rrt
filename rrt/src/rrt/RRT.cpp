#include "RRT.h"
#include <cmath>

#include "edges/StraightEdge.h"

RRT::RRT() {
	nullEdge = Edge::newEdge;
	newEdge = StraightEdge::newEdge;
}

RRT::~RRT() {

}

void RRT::initialize(Position fixed) {
	flexibleNode = nullptr;
	graph = std::unique_ptr<Graph>(newGraph()); //neuen Graph erstellen
	fixedNode = new Node(fixed, nullptr, 0, nullEdge);
	graph->addNode(fixedNode);
	ROS_INFO_STREAM("Wurzel-Position: " << fixed);
	custominit();
}

bool RRT::finishPlanning() {
	Node* tempNode = findAndSetParentForFlexible(getFlexible());
	if (tempNode != nullptr) {
		if (flexibleNode == nullptr || tempNode->getCost() < flexibleNode->getCost()) {
			flexibleNode = tempNode;
			graph->addNode(flexibleNode);
		}
		if (flexibleNode != tempNode) {
			delete tempNode;
		}
	}

	ROS_INFO_STREAM("Flexible-Position: " << getFlexible());

	if (flexibleNode != nullptr) {
		ROS_INFO_STREAM("Fertig, pfad gefunden");
		return true;
	} else {
		ROS_INFO_STREAM("Fertig, kein pfad gefunden");
		return false;
	}
}

void RRT::rewire(Node* newNode) {
	// für überschreiben in darunterliegenden Funktionen
	// darf den Knoten nicht mehr verschieben!
	// und auch nur eltern verbinden, sodass es zu keiner Kollision kommt
}

/**
 * fliexible: flexibler Knoten, ein Ende des gesuchten Weges
 * fixed: fixierter Knoten, Wurzel des Baumes, anderes Ende des gesuchten Weges
 * resetplan: soll der Graph zurückgesetzt werden oder weitergemacht werden?
 *
 */
bool RRT::planPath(int nodes) {
	int targetnodes = nodes + graph->size();
	// TODO weicher statt harter Grenze?
	// TODO Sampler wieder woanders extrahieren?
	world->setBoundary(fixedNode->getPosition(), getFlexible());
	// TODO stopwhenpathfound?
	while (graph->size() < targetnodes) {
		addNewNode();
	}

	return finishPlanning();
}

Node* RRT::addNewNode() {
	Node* newNode = generateNewNodeWithParentAndMove();
	if (!newNode->skipCheck() && (hasCollision(newNode) || hasCollision(newNode->getEdge()))) {
		delete newNode;
		return nullptr;
	}
	graph->addNode(newNode);
	prepareRewire(newNode);
	rewire(newNode);
	finishRewire(newNode);
	return newNode;
}

Node* RRT::generateNewNodeWithParentAndMove() {
	return generateNewNodeWithParentAndMoveDefault();
}

Position RRT::generateNewNode() {
	return generateNewNodeByRandom();
}

Node* RRT::findAndSetParent(Position newPosition) {
	return setParentToNearestNode(newPosition);
}

/**
 * Falls hier die Kante verschoben wird, wird trotzdem die Kantenkollision nochmall in addNode geprüft
 */
Node* RRT::moveNewNode(Node* newNode) {
	newNode->getEdge()->setConstraints();
	return newNode;
}

Position RRT::generateNewNodeByRandom() {
	return world->sample();
}

Node* RRT::setParentToNearestNode(Position newPosition) {
	NodeWithDistance nearestNodeWithDistance = graph->getNearestNodeWithDistance(&newPosition);
	Node* newNode = new Node(newPosition, nearestNodeWithDistance.node, nearestNodeWithDistance.distance, newEdge);

	return newNode;
}

const char* RRT::getName() {
	return "RRT";
}

void RRT::prepareRewire(Node* newNode) {
	// Vorbereitung Rewire, wird nur auf den neuen Knoten vorm Rewire aufgerufen
}

Node* RRT::findAndSetParentForFlexible(Position newPosition) {
	return checkOrDeleteNode(findAndSetParent(newPosition));
}

Node* RRT::checkOrDeleteNode(Node* tempNode) {
	if (tempNode != nullptr) {
		if (tempNode->getEdge() != nullptr && tempNode->getEdge()->isInConstraints()
				&& !hasCollision(tempNode->getEdge()) && !hasCollision(tempNode)) {
			return tempNode;
		} else {
			delete tempNode;
		}
	}
	return nullptr;
}

void RRT::finishRewire(Node* newNode) {
	// Nachbereitung Rewire,  wird nur auf den neuen Knoten nach dem Rewire aufgerufen
}

Node* RRT::generateNewNodeWithParentAndMoveDefault() {
	Position newPosition = generateNewNode();
	Node* newNode = findAndSetParent(newPosition);
	moveNewNode(newNode);
	return newNode;
}

void RRT::custominit() {
}
