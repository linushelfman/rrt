#ifndef RRT_RRTFINDBESTANCESTOR_H_
#define RRT_RRTFINDBESTANCESTOR_H_

#include "RRTFunctions.h"

class RRTFindBestAncestor : public virtual RRTFunctions {
public:
	RRTFindBestAncestor();
	virtual ~RRTFindBestAncestor();
protected:
	Node* findBestAncestorAsParent(Position newPosition);
};

#endif /* RRT_RRTFINDBESTANCESTOR_H_ */
