#ifndef RRT_INFORMEDRRT_H_
#define RRT_INFORMEDRRT_H_

#include "RRTFunctions.h"

class InformedRRT: public virtual RRTFunctions {
public:
	InformedRRT();
	virtual ~InformedRRT();
protected:
	Position sampleNodeForInformedRRT();
	bool useInformedRRTSampler();
private:
	double yawToFixed = NAN;
	double distanceToFixed = NAN;
	Position center;
};

#endif /* RRT_INFORMEDRRT_H_ */
