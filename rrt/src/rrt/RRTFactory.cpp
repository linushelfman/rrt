#include "RRTFactory.h"

RRTFactory::RRTFactory(std::string name, bool doRewire, bool useCache, bool informed, EdgeInfo edgeInfo, graphGen graphFunc) {
	this->name = name;
	this->doRewire = doRewire;
	this->useCache = useCache;
	this->newEdge = edgeInfo.edgeFunc;
	this->informed = informed;
	this->smart = false;
	this->hybrid = false;
	this->findbestAncestorAsParent = false;
	MAX_EDGE_LENGTH = edgeInfo.maxEdgeLength;
	this->newGraph = graphFunc;
}

RRTFactory::~RRTFactory() {

}

const char* RRTFactory::getName() {
	return name.c_str();
}

void RRTFactory::rewire(Node* newNode) {
	if (doRewire) {
		rewireWithNearbyList(newNode, useCache, true);
	}
}

void RRTFactory::finishRewire(Node* newNode) {
	checkIfPathFound(newNode, hybrid);
}

Position RRTFactory::generateNewNode() {
	if (informed && useInformedRRTSampler()) {
		return sampleNodeForInformedRRT();
	} else {
		return generateNewNodeByRandom();
	}
}

void RRTFactory::setHybrid() {
	hybrid = true;
}

void RRTFactory::setSmart() {
	smart = true;
}

Node* RRTFactory::findAndSetParentForFlexible(Position newPosition) {
	if (hybrid) {
		return findHybridParentForPos(newPosition);
	} else {
		return checkOrDeleteNode(findAndSetParent(newPosition));
	}
}

std::shared_ptr<Planner> RRTFactory::getRRTForTests(bool hybrid, bool informed, bool smart,
		bool bestAncestor) {
	std::string name = "RRT*";
	if (informed) {
		name.append("I");
	}
	if (smart) {
		name.append("S");
	}
	if (hybrid) {
		name.append("H");
	}
	if (bestAncestor) {
		name.append("BA");
	}
	RRTFactory* rrt = new RRTFactory(name, true, true, informed, SteeringEdge::getEdgeInfo());
	if (bestAncestor) {
		rrt->setFindBestAncestor();
	}
	if (smart) {
		rrt->setSmart();
	}
	if (hybrid) {
		rrt->setHybrid();
	}
	return std::shared_ptr<Planner>(rrt);;
}

Node* RRTFactory::generateNewNodeWithParentAndMove() {
	if (smart && useRRTSmartSampler()) {
		return generateNodeForRRTSmart();
	} else {
		return generateNewNodeWithParentAndMoveDefault();
	}
}

void RRTFactory::setFindBestAncestor() {
	findbestAncestorAsParent = true;
}

void RRTFactory::custominit() {
	initFunctions();
	initSmart();
}

Node* RRTFactory::findAndSetParent(Position newPosition) {
	if (findbestAncestorAsParent) {
		return findBestAncestorAsParent(newPosition);
	} else {
		return setParentToNearestNode(newPosition);
	}
}
