#include "RRTFindBestAncestor.h"

RRTFindBestAncestor::RRTFindBestAncestor() {

}

RRTFindBestAncestor::~RRTFindBestAncestor() {

}

/**
 * Problem: findAndSetParent darf nicht verschieben, weil man damit den Parent fürs Target holt
 * Wir wollen aber unsern echten Parent erst nach dem Move festlegen, da es sonst evtl. plötzlich einen besseren gibt
 * ... => in findAndSetParent probieren einen Parent zu finden, falls nicht möglich, auf den besten Parent setzen und dies irgentwie kenntlich machen
 * => moveNewNode auch überschreiben
 * => findAndSetParent aufruf in planPath entsprechend abfangen
 */
Node* RRTFindBestAncestor::findBestAncestorAsParent(Position newPosition) {
	// diese Stelle so umbauen, dass sie stattdessen die nächsten Knoten durchgeht und anhand deren Kurven prüft, ob diese Kurven uns erreichen können
	// falls es keine gibt die uns erreichen kann, stattdessen die Kurve nehmen, bei der wir am wenigsten verschoben werden müssen
	// oder die Kurve, wo wir am weitesten von anderen entfernt sind, d.h. gute streuung erreichen

	// umbauen sodass man gleichzeitig die Knoten in Radius X und den allernächsten Knoten holt
	// und falls im Radius X keine Knoten ist, soll er automatisch 2X testen...? => Datenstruktur

	NearbyData nearbyData = graph->getNearbyData(&newPosition, MAX_EDGE_LENGTH,
			Edge::MIN_TURNING_RADIUS);
	Node* newNode = new Node(newPosition, nearbyData.nearestNode.node,
			nearbyData.nearestNode.distance, newEdge);

	std::vector<std::shared_ptr<Edge>> outgoingEdges;
	for (NodeWithDistance& node : nearbyData.nodesInDistance) {
		outgoingEdges.push_back(newEdge(newNode, node.node, node.distance));
	}

	std::shared_ptr<Edge> possibleEdge; // d.h. echt mögliche Elternkurve
	std::shared_ptr<Edge> theoreticalAncestorEdge; // d.h. theoretisch möglicher Vorfahr, es bräuchte aber noch Knoten auf der Verbindung

	for (std::shared_ptr<Edge>& newOutgoingEdge : outgoingEdges) {
		// umdrehen weil wir Kurven zu uns haben wollen
		std::shared_ptr<Edge> newIncomingEdge = newOutgoingEdge->getReverse();
		// 2 Variablen, einmal die bestmögliche Kurve
		// und einmal die theoretisch bestmögliche Kurve, unter ignorieren der max. Kantenlänge und evtl. anderer Faktoren

		if (newIncomingEdge->isInAncestorConstraints()
				&& (possibleEdge == nullptr
						|| newIncomingEdge->getCostOfEnd() < possibleEdge->getCostOfEnd())) {
			possibleEdge = newIncomingEdge;
		}
		if (possibleEdge == nullptr
				&& (newIncomingEdge->isInAncestorConstraints()
						&& (theoreticalAncestorEdge == nullptr
								|| newIncomingEdge->getCostOfEnd()
										< theoreticalAncestorEdge->getCostOfEnd()))) {
			theoreticalAncestorEdge = newIncomingEdge;
		}
	}

	if (possibleEdge != nullptr) {
		newNode->setEdge(possibleEdge);
		newNode->setOutgoingEdges(outgoingEdges);
	} else {
		std::shared_ptr<Edge> theoreticalAncestorEdge3;
		for (NodeWithDistance& node : nearbyData.nodesInAncestorDistance) {
			std::shared_ptr<Edge> newIncomingEdge = newEdge(node.node, newNode, node.distance);
			if (newIncomingEdge->isInAncestorConstraints()
					&& (theoreticalAncestorEdge3 == nullptr
							|| newIncomingEdge->getCostOfEnd()
									< theoreticalAncestorEdge3->getCostOfEnd())) {
				theoreticalAncestorEdge3 = newIncomingEdge;
			}
		}

		if (theoreticalAncestorEdge == nullptr
				|| (theoreticalAncestorEdge3 != nullptr
						&& theoreticalAncestorEdge3->getCostOfEnd()
								< theoreticalAncestorEdge->getCostOfEnd())) {
			theoreticalAncestorEdge = theoreticalAncestorEdge3;
		}

		if (theoreticalAncestorEdge != nullptr) {
			newNode->setEdge(theoreticalAncestorEdge);
		}

	}
	return newNode;
}
