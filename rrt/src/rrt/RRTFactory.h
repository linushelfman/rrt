#ifndef RRT_RRTFACTORY_H_
#define RRT_RRTFACTORY_H_

#include "InformedRRT.h"
#include "RRTSmart.h"
#include "RRTStar.h"
#include "RRTFindBestAncestor.h"
#include "edges/StraightEdge.h"
#include "edges/SteeringEdge.h"
#include "edges/DubinsEdge.h"

class RRTFactory: public RRTStar, public InformedRRT, public RRTSmart, public RRTFindBestAncestor {
public:
	RRTFactory(std::string name, bool doRewire, bool useCache, bool informed, EdgeInfo edgeInfo = StraightEdge::getEdgeInfo(), graphGen graphFunc = BoxGraph::getGraphGen(1.0));
	static std::shared_ptr<Planner> getRRTForTests(bool hybrid, bool informed, bool smart, bool bestAncestor);
	virtual ~RRTFactory();
	virtual const char* getName();
protected:
	void rewire(Node* newNode);
	void finishRewire(Node* newNode);
	Position generateNewNode();
	Node* findAndSetParentForFlexible(Position newPosition);
	Node* findAndSetParent(Position newPosition);
	Node* generateNewNodeWithParentAndMove();
	void custominit();
private:
	void setHybrid();
	void setSmart();
	void setFindBestAncestor();


	std::string name;
	bool doRewire;
	bool useCache;
	bool informed;
	bool smart;
	bool hybrid;
	bool findbestAncestorAsParent;
};

#endif /* RRT_RRTFACTORY_H_ */
