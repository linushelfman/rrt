#include "Planner.h"
#include <tf/tf.h>

Planner::Planner() {

}

Planner::~Planner() {
	// alles unique-ptr => löscht sich selbst
}

void Planner::addObstaclePoint(double x, double y, double radius) {
	obstacleManager->addObstaclePoint(x,y,radius);
}

void Planner::planPathWithCarPos(Position target) {
	planPathWithCarPos(target, START_NODES);
}

void Planner::planPathWithCarPos(Position target, int nodes) {
	this->autoUpdateCarPos = true;
	planPath(car->getPosition(), target, true, nodes);
}

void Planner::planPath(Position start, Position target) {
	planPath(start, target, true);
}

void Planner::planPath(Position start, Position target, bool fixedTarget) {
	planPath(start, target, fixedTarget, START_NODES);
}

void Planner::planPath(Position start, Position target, bool fixedTarget, int nodes) {
	initialized = true;
	targetIsFixed = fixedTarget;
	if (fixedTarget) {
		initialize(target);
		flexiblePos = start;
	} else {
		initialize(start);
		flexiblePos = target;
	}
	replanPath(flexiblePos, nodes);
}

void Planner::replanPath() {
	replanPath(getFlexible(), REPLAN_NODES);
}

void Planner::replanPath(Position flexible) {
	replanPath(flexible, REPLAN_NODES);
}

void Planner::replanPath(int nodes) {
	replanPath(getFlexible(), nodes);
}

void Planner::replanPath(Position flexible, int nodes) {
	if (!initialized) {
		return;
	}
	flexiblePos = flexible;
	pathfound = planPath(nodes);
	path = nullptr;
}

Position Planner::getFlexible() {
	// TODO autoUpdateCarPos testen
	// Latenz messen/prüfen/schätzen
	if (autoUpdateCarPos) {
		return car->getPosition();
	} else {
		return flexiblePos;
	}
}

void Planner::calcPath() {
	if (pathfound) {
		path = std::unique_ptr<Path>(graph->followPrevFrom(flexibleNode));
		if (targetIsFixed) {
			path->reverse();
		}
	}
}

fub_trajectory_msgs::Trajectory Planner::getTrajectoryMsg() {
	if (!pathfound) {
		return fub_trajectory_msgs::Trajectory();
	}
	if (path == nullptr) {
		calcPath();
	}
	return path->toTrajectoryMsg();
}

nav_msgs::Path Planner::getPathMsg() {
	if (!pathfound) {
		return nav_msgs::Path();
	}
	if (path == nullptr) {
		calcPath();
	}
	return path->toPathMsg();
}

rrt_msgs::Graph Planner::getRRTGraph() {
	return graph->getGraphMsg();
}

rrt_msgs::Obstacles Planner::getObstacles() {
	return obstacleManager->getObstaclesMsg();
}

bool Planner::hasCollision(Node* newNode) {
	return obstacleManager->hasCollision(*newNode);
}

bool Planner::hasCollision(std::shared_ptr<Edge> newEdge) {
	return obstacleManager->hasCollision(newEdge->getCurves());
}

const char* Planner::getName() {
	return "Planner";
}

bool Planner::foundPath() {
	return pathfound;
}

void Planner::addObstacleWall(double x1, double y1, double x2, double y2, double thickness) {
	obstacleManager->addObstacle(
			std::min<double>(x1, x2) - fabs(thickness / 2),
			std::min<double>(y1, y2) - fabs(thickness / 2),
			std::max<double>(x1, x2) + fabs(thickness / 2),
			std::max<double>(y1, y2) + fabs(thickness / 2));
}

double Planner::getCost() {
	if (flexibleNode != nullptr) {
		return flexibleNode->getCost();
	} else {
		return NAN;
	}
}
