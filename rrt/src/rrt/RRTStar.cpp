#include "RRTStar.h"

RRTStar::RRTStar() {

}

RRTStar::~RRTStar() {

}

void RRTStar::rewireWithNearbyList(Node* nodeToRewire, bool useCache, bool checkOwnParent) {
	std::vector<std::shared_ptr<Edge>> outgoingEdges = calculateOutgoingEdgesFor(nodeToRewire,
			useCache);

	if (checkOwnParent) {
		std::shared_ptr<Edge> bestEdge = nodeToRewire->getEdge();
		for (std::shared_ptr<Edge> newEdge : outgoingEdges) {
			// umdrehen weil wir Kurven zu uns haben wollen
			newEdge = newEdge->getReverse();

			if (newEdge != bestEdge && newEdge->isInConstraints()
					&& newEdge->getCostOfEnd() < bestEdge->getCostOfEnd()) {
				bestEdge = newEdge;
			}
		}
		nodeToRewire->setEdge(bestEdge);
	}

	for (std::shared_ptr<Edge>& newEdge : outgoingEdges) {
		if (newEdge->isInConstraints()
				&& newEdge->getCostOfEnd() + MIN_COST_REDUCTION_FOR_REWIRING
						< newEdge->getEnd()->getCost()) {
			// dieser getCost check schlägt manchfal mit segfault fehl
			newEdge->getEnd()->setEdge(newEdge);
			// TODO umbauen das man hier doch rewire direkt aufrufen kann?
			rewireWithNearbyList(newEdge->getEnd(), useCache, false);
		}
	}
}

std::vector<std::shared_ptr<Edge>> RRTStar::calculateOutgoingEdgesFor(Node* node, bool useCache) {
	std::vector<std::shared_ptr<Edge>> outgoingEdges;
	if (useCache && node->hasOutgoingEdges()) {
		return node->getOutgoingEdges();
	}
	for (NodeWithDistance& otherNode : graph->getNodesNear(node, MAX_EDGE_LENGTH)) {
		std::shared_ptr<Edge> edge = newEdge(node, otherNode.node, otherNode.distance);
		if (!hasCollision(edge)) {
			outgoingEdges.push_back(edge);
		}
	}
	if (useCache && !node->hasOutgoingEdges()) {
		node->setOutgoingEdges(outgoingEdges);
	}
	return outgoingEdges;
}
