#ifndef SRC_RRTSTAR_H_
#define SRC_RRTSTAR_H_

#include "RRT.h"

class RRTStar : public virtual RRT {
public:
	RRTStar();
	virtual ~RRTStar();
protected:
	std::vector<std::shared_ptr<Edge>> calculateOutgoingEdgesFor(Node* node, bool useCache = true);
	void rewireWithNearbyList(Node* nodeToRewire, bool useCache, bool checkOwnParent);
};

#endif /* SRC_RRTSTAR_H_ */
