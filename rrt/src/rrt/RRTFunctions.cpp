#include "RRTFunctions.h"
#include "edges/DubinsEdge.h"
#include "edges/SteeringEdge.h"

RRTFunctions::RRTFunctions() {
	initFunctions();
}

RRTFunctions::~RRTFunctions() {

}

void RRTFunctions::checkIfPathFound(Node* newNode, bool setIfFound) {
	double distance = getFlexible().distanceTo(newNode);
	if (distance < CHECK_PARENT_RADIUS) {
		std::unique_ptr<Node> testNode = std::unique_ptr<Node>(getHybridPathOverNode(newNode));
		if (testNode) {
			if (testNode->getCost() < shortestPath) {
				shortestPath = testNode->getCost();
				nearestNodeToFlexible = newNode;
				possiblepathfound = true;
				if (flexibleNode == nullptr && setIfFound) {
					flexibleNode = addNodesFromEdgeToNewNode(testNode->getEdge(), MAX_EDGE_LENGTH);
				}
			}
		}
	}
}

Node* RRTFunctions::findHybridParentForPos(Position newPosition) {
	Node* bestNode = nullptr;
	for (NodeWithDistance& nodeWithDistance : graph->getNodesNear(&newPosition, CHECK_PARENT_RADIUS)) {
		Node* testNode = getHybridPathOverNode(nodeWithDistance.node);
		if (testNode != nullptr) {
			if (bestNode == nullptr) {
				bestNode = testNode;
			} else if (testNode->getCost() < bestNode->getCost()) {
				delete bestNode;
				bestNode = testNode;
			}
		}
	}
	return bestNode;
}

Node* RRTFunctions::getHybridPathOverNode(Node* newNode) {
	return checkOrDeleteNode(new Node(getFlexible(), newNode, getFlexible().distanceTo(newNode), DubinsEdge::newEdge));
}

Node* RRTFunctions::addNodesFromEdgeToNewNode(std::shared_ptr<Edge> edge, double maxEdgeLength) {
	Node* lastNode = edge->getStart();
	for (Position pos : getPositionsFromEdge(edge, maxEdgeLength)) {
		lastNode = new Node(pos, lastNode, pos.distanceTo(lastNode), SteeringEdge::newEdge);
		graph->addNode(lastNode);
	}
	if (lastNode == edge->getStart()) {
		return nullptr;
	} else {
		return lastNode;
	}
}

std::vector<Position> RRTFunctions::getPositionsFromEdge(std::shared_ptr<Edge> edge,
		double maxEdgeLength) {
	std::vector<Position> positions;
	for (std::shared_ptr<Curve> curve : edge->getCurves()) {
		for (Position pos : curve->calculatePointsOnCurve(maxEdgeLength)) {
			positions.push_back(pos);
		}
	}
	return positions;
}

void RRTFunctions::initFunctions() {
	possiblepathfound = false;
	nearestNodeToFlexible = nullptr;
	shortestPath = 1e10;
}
