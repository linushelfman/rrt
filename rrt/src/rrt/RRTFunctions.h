#ifndef SRC_RRTFUNCTIONS_H_
#define SRC_RRTFUNCTIONS_H_

#include "RRT.h"

// RRT* Hybrid
class RRTFunctions : public virtual RRT {
public:
	RRTFunctions();
	virtual ~RRTFunctions();
protected:
	void checkIfPathFound(Node* newNode, bool setIffound);
	Node* findHybridParentForPos(Position newPosition);
	Node* getHybridPathOverNode(Node* newNode);
	Node* addNodesFromEdgeToNewNode(std::shared_ptr<Edge> edge, double maxEdgeLength);
	std::vector<Position> getPositionsFromEdge(std::shared_ptr<Edge> edge, double maxEdgeLength);
	void initFunctions();

	bool possiblepathfound;
	Node* nearestNodeToFlexible;
	double shortestPath;
	double CHECK_PARENT_RADIUS = 1;
};

#endif /* SRC_RRTFUNCTIONS_H_ */
