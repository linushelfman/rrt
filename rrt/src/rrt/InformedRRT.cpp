#include "InformedRRT.h"
#include "edges/Edge.h"

InformedRRT::InformedRRT() {

}

InformedRRT::~InformedRRT() {

}

Position InformedRRT::sampleNodeForInformedRRT() {
	// Ellipse aus getFlexible(), fixed und shortestPath

	// TODO: Ellipserandom extrahieren
	// Vektor von flexible nach Center bzw. center nach fixed
	if (std::isnan(yawToFixed)) {
		distanceToFixed = getFlexible().distanceTo(fixedNode);
		yawToFixed = getFlexible().angleTo(fixedNode);
		center = getFlexible().getPosWithDirectionDistanceYaw(yawToFixed, distanceToFixed / 2.0,
				yawToFixed);
	}
	std::random_device generator;
	std::uniform_real_distribution<double> distributionR(0, 1);
	std::uniform_real_distribution<double> distributionAlpha(-M_PI, M_PI);

	double r = distributionR(generator);
	double alpha = distributionAlpha(generator);
	// jetzt mit höhe/breite der Ellipse multiplizieren
	// und danach in Richtung der Ellipse drehen!
	// Breite = Radius1+radius2 = 2a = pathCost!
	double width = shortestPath;
	// Höhe: Rechtwinkliges Dreieck aus a=widht/2, b=height/2, dist/2 wobei hypotenus a ist
	double height = sqrt(width * width - distanceToFixed * distanceToFixed);
	// Punkte berechnen und strecken
	double x = sqrt(r) * cos(alpha) * width / 2.0;
	double y = sqrt(r) * sin(alpha) * height / 2.0;

	// zurück in winkel koordinaten und alpha drehen
	// als punkt berechnen und zurückgeben
	return center.getPosWithDirectionDistanceYaw(atan2(y, x) + yawToFixed, hypot(x, y),
			distributionAlpha(generator));
}

bool InformedRRT::useInformedRRTSampler() {
	return possiblepathfound;
}
