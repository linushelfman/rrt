#include "RRTSmart.h"
#include "edges/DubinsEdge.h"
#include <algorithm>
#include "edges/SteeringEdge.h"

RRTSmart::RRTSmart() {
	initSmart();
}

RRTSmart::~RRTSmart() {

}

void RRTSmart::initSmart() {
	currentNumber = 0;
	positions = std::list<Position>();
	parentNodeForSmart = nullptr;
	lastoptimizedCost = 1e10;
	lastOptimizedNode = nullptr;
	lastfound = false;
	lastWasFlexible = false;
}

void RRTSmart::sampleNodeForRRTSmart() {
	// TODO die ersten 15 Zeilen hiervor extrahieren und für useRRTSmartSampler nutzen!
	if (positions.empty()) {
		Node* toOptimize;
		if (flexibleNode != nullptr) {
			toOptimize = flexibleNode;
			// für den Fall das wir von nearestNode zu flexiblNode wechseln
			if (!lastWasFlexible) {
				lastoptimizedCost = flexibleNode->getCost() + 1;
			}
			lastWasFlexible = true;
		} else {
			toOptimize = nearestNodeToFlexible;
		}
		// falls wir im letzten durchgang was gefunden haben, unsere Strecke kürzer geworden ist (d.h. es hat sich was geändert) oder der zu optimierende Knoten sich geändert hat
		// dann optimiern wir wieder
		if (lastfound || toOptimize->getCost() + 0.00001 <= lastoptimizedCost
				|| lastOptimizedNode != toOptimize) {
			lastfound = false;
			lastOptimizedNode = toOptimize;
			lastoptimizedCost = toOptimize->getCost();
			std::vector<Node*> pathToOptimize = graph->getPathTo(toOptimize);

			unsigned int stepsize = std::min<unsigned int>(pathToOptimize.size() - 1, MAX_STEPSIZE);
			// neue Punkte generieren falls keine vorhanden
			// TODO ! evtl. nicht immer von MIN_STEPSIZE starten sondern auch mal von max starten?
			// bzw. einfach die Variablen cachen!
			while (positions.empty() && stepsize >= MIN_STEPSIZE) {
				// probieren punkte zu generieren
				unsigned int i = 0;
				while (i + stepsize < pathToOptimize.size()) {
					Node* start = pathToOptimize[i];
					Node* end = pathToOptimize[i + stepsize];
					std::unique_ptr<Node> tempNode = std::unique_ptr<Node>(
							checkOrDeleteNode(
									new Node(end->getPosition(), start, start->distanceTo(end),
											DubinsEdge::newEdge)));
					if (tempNode != nullptr && tempNode->getCost() + MIN_COST_REDUCTION_FOR_REWIRING
					< end->getCost()) {
						ROS_DEBUG_STREAM(
								"Verbesserung von Stelle " << i << " bis " << i+stepsize << " gefunden: "
								<< end->getCost() - start->getCost() << " -> " << tempNode->getCost()- start->getCost());
						parentNodeForSmart = start;
						for (Position pos : getPositionsFromEdge(tempNode->getEdge(),
								MAX_EDGE_LENGTH * 0.999)) { // gegen Rundungsfehler, das es beim rewire gefunden wird
							positions.push_back(pos);
						}
						// den letzten Knoten entfernen, da er ja bereits existiert
						// der 1. wurde nie berchnet
						positions.pop_back();
						lastfound = true;
						break;
					}
					i++;
				}
				stepsize--;
			}
		}
	}
}

bool RRTSmart::useRRTSmartSampler() {
	// so zählt er nur hoch wenn pathfound, Problem: Overflow...
	return possiblepathfound && (currentNumber++ % BIAS_RATIO) == 0;
}

Node* RRTSmart::generateNodeForRRTSmart() {
	if (positions.empty()) {
		sampleNodeForRRTSmart();
	}

	if (!positions.empty()) {
		// Punkte von vorne entfernen, in reihenfolge...
		Position newPosition = positions.front();
		positions.pop_front();

		parentNodeForSmart = new Node(newPosition, parentNodeForSmart,
				newPosition.distanceTo(parentNodeForSmart), newEdge);

		// sonst wird der Knoten evtl. gelöscht und es funktioniert gar nichts mehr!
		parentNodeForSmart->setSkipCheck();

		return parentNodeForSmart;
	} else {
		//		ROS_INFO_STREAM("RRTSmart konnte nichts finden und generiert daher zufällig");
		// nur falls ich es nicht schaffe punkte zu generieren, sample aufrufen
		// das funktioniert nur wenn bias != 1, aber das sollte es ja sein
		return generateNewNodeWithParentAndMove();
	}
}
