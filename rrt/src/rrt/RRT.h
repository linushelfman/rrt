#ifndef SRC_RRT_H_
#define SRC_RRT_H_

#include "../data/BoxGraph.h"
#include "Planner.h"
#include "data/VectorGraph.h"
#include "data/BoxGraph.h"

class RRT: public Planner {
public:
	RRT();
	~RRT();

	virtual const char* getName();
protected:
	void initialize(Position fixed);
	bool planPath(int nodes);

	// Hilfsfunktionen
	Position generateNewNodeByRandom();
	Node* setParentToNearestNode(Position newPosition);
	bool finishPlanning();
	Node* checkOrDeleteNode(Node* tempNode);
	Node* generateNewNodeWithParentAndMoveDefault();

	// virtuelle Funktionen
	virtual Node* addNewNode();
	virtual Node* generateNewNodeWithParentAndMove();
	virtual Position generateNewNode();
	virtual Node* findAndSetParent(Position newPosition);
	virtual Node* findAndSetParentForFlexible(Position newPosition);
	virtual Node* moveNewNode(Node* newNode);
	virtual void rewire(Node* newNode);
	virtual void prepareRewire(Node* newNode);
	virtual void finishRewire(Node* newNode);
	virtual void custominit();



	// Variablen
	newEdgeFunc nullEdge;
	newEdgeFunc newEdge;
	graphGen newGraph = BoxGraph::getGraphGen();
	double MAX_EDGE_LENGTH = 0.3;
	double MIN_COST_REDUCTION_FOR_REWIRING = 0.01; // nicht für kleine verbesserungen/Rundungsunterschiede ganz viel rewiring betreiben, auch für RRTSmart
};
#endif /* SRC_RRT_H_ */
