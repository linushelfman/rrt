
class Planner;

#ifndef SRC_PLANNER_H_
#define SRC_PLANNER_H_

#include <fub_trajectory_msgs/Trajectory.h>
#include "world/World.h"
#include "data/Path.h"
#include "world/Car.h"
#include "data/Graph.h"
#include "world/ObstacleManager.h"
#include <ros/ros.h>

/**
 * Basisklasse für Planer die einen Weg berechnen
 * Beinhaltet Standardfunktionen
 */
class Planner {
public:
	Planner();
	virtual ~Planner();

	// Allgemein
	void planPathWithCarPos(Position target);
	void planPathWithCarPos(Position target, int nodes);
	void planPath(Position start, Position target);
	void planPath(Position start, Position target, bool fixedTarget);
	void planPath(Position start, Position target, bool fixedTarget, int nodes);
	void replanPath();
	void replanPath(Position flexible);
	void replanPath(int nodes);
	void replanPath(Position flexible, int nodes);
	bool foundPath();
	virtual const char* getName();

	// TODO umbauen, sodass die einzelnen Objekte es einzeln Broadcasten?
	// und man bloß den Befehl zum Broadcasten durchreicht?
	nav_msgs::Path getPathMsg();
	fub_trajectory_msgs::Trajectory getTrajectoryMsg();
	rrt_msgs::Graph getRRTGraph();
	rrt_msgs::Obstacles getObstacles();

	// Hindernisse
	void addObstaclePoint(double x, double y, double radius = 0.25);
	void addObstacleWall(double x1, double y1, double x2, double y2, double thickness = 0.2);

	double getCost();
protected:
	// Funktionen zum überschreiben
	virtual void initialize(Position fixed) = 0;
	virtual bool planPath(int nodes) = 0;

	// Hilfsfunktionen
	bool hasCollision(Node* newNode);
	bool hasCollision(std::shared_ptr<Edge> newEdge);
	Position getFlexible();

	// Variablen
	Node* flexibleNode = nullptr;
	Node* fixedNode = nullptr;

	std::unique_ptr<Graph> graph;
	std::unique_ptr<World> world = std::unique_ptr<World>(new World());

private:
	int START_NODES = 3000;
	int REPLAN_NODES = 500;

	Position flexiblePos;

	bool initialized = false;
	bool targetIsFixed = false;
	bool autoUpdateCarPos = false;
	bool pathfound = false;

	std::unique_ptr<Path> path;
	std::unique_ptr<Car> car = std::unique_ptr<Car>(new Car());
	std::unique_ptr<ObstacleManager> obstacleManager = std::unique_ptr<ObstacleManager>(
			new ObstacleManager());

	void calcPath();

};

#endif /* SRC_PLANNER_H_ */
