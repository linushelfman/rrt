#ifndef TEST_PROFILES_H_
#define TEST_PROFILES_H_

#include<rrt/Planner.h>

enum Profile {
	EMPTY,
	PERF,
	LABYRINTH,
	OBSTACLES,
	PARKING1,
	PARKING2
};

std::pair<Position,Position> setScene(std::shared_ptr<Planner> planner, Profile p);
std::pair<Position,Position> setSceneEmpty(std::shared_ptr<Planner> planner);
std::pair<Position,Position> setScenePerf(std::shared_ptr<Planner> planner);
std::pair<Position,Position> setSceneLabyrinth(std::shared_ptr<Planner> planner);
std::pair<Position,Position> setSceneObstacles(std::shared_ptr<Planner> planner);
std::pair<Position,Position> setSceneParking1(std::shared_ptr<Planner> planner);
std::pair<Position,Position> setSceneParking2(std::shared_ptr<Planner> planner);

#endif /* TEST_PROFILES_H_ */
