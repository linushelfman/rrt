#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int16.h>
#include <tf/tf.h>

#include "wrapperlib.h"

enum State {
	ready, //wartet auf Befehl
	planning, //ist gerade am planen
	following //ist gerade am folgen
};

enum Command {
	nop, //no operation (sleep und wait sind vergeben...)
	plan, // planen starten
	stop // planen/folgen beenden und stehenbleiben => evtl. leeren Pfad puschen damit Controller stehenbleibt?
};

bool DEBUG = false;
Command command = nop;
State state = ready;
bool repub = false;

Position target;
int planner;

/*
 * erstmal so, später evtl. als Service?
 */
void cmdProcessor(const std_msgs::String::ConstPtr& cmd) {
	ROS_INFO("new CMD '%s'", cmd->data.c_str());
	if (cmd->data.compare("stop") == 0 || command == stop) {
		command = stop;
	} else if (cmd->data.size() >= 4 && cmd->data.substr(0,4).compare("plan") == 0) {
		command = plan;
		if (cmd->data.size() > 6) {
			planner = atoi(cmd->data.substr(5,1).c_str());
		}
	}
}

void goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg) {
	tf::Pose pose;
	tf::poseMsgToTF(msg->pose, pose);
	target.setPosition(msg->pose.position.x, msg->pose.position.y, tf::getYaw(pose.getRotation()));
	ROS_INFO_STREAM("Target: " << target);
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "rrt_wrapper");
	ros::NodeHandle n;

	ros::Publisher pathPublisher = n.advertise<fub_trajectory_msgs::Trajectory>("/planned_path", 1);
	ros::Publisher rrtGraphPublisher = n.advertise<rrt_msgs::Graph>("/rrt/graph", 1);
	ros::Publisher rrtPathPublisher = n.advertise<nav_msgs::Path>("/rrt/path", 1);
	ros::Publisher obstaclePublisher = n.advertise<rrt_msgs::Obstacles>("/rrt/obstacles", 1);
	ros::Publisher speedPublisher = n.advertise<std_msgs::Int16>("/manual_control/speed", 1);

	ros::Subscriber cmdSubscriber = n.subscribe("/rrt/cmd", 10, cmdProcessor);
	ros::Subscriber goalSubscriber = n.subscribe("/move_base_simple/goal", 1, goalCallback);

	std::vector<std::shared_ptr<Planner>> planners = getPlanners();
	planner = 4;
	target = Position(0,0,0);
	int addNodes = 100;
	int replanningrate = 10;

	fub_trajectory_msgs::Trajectory trajectory;
	nav_msgs::Path path;
	rrt_msgs::Graph rrt_graph;
	rrt_msgs::Obstacles obstacles;

	std_msgs::Int16 speed;
	speed.data = 0;

	// 1Hz
	ros::Rate rate(replanningrate);
	ROS_INFO("RRT Wrapper started");

	while (ros::ok()) {
		switch (command) {
		case plan:
			command = nop;
			state = planning;
			ROS_INFO("Planing...");
			planners[planner]->planPathWithCarPos(target);
			while (!planners[planner]->foundPath()) {
				ROS_INFO_STREAM("No Path found, adding " << addNodes << " Nodes");
				planners[planner]->replanPath(addNodes);
				ros::spinOnce();
				if (command == stop) {
					break;
				}
			}
			ROS_INFO("Generate Messages...");
			state = following;
			trajectory = planners[planner]->getTrajectoryMsg();
			if (DEBUG) {
				rrt_graph = planners[planner]->getRRTGraph();
			}
			path = planners[planner]->getPathMsg();
			obstacles = planners[planner]->getObstacles();
			ROS_INFO("Planning finished.");
			ROS_INFO("MEMORY %d", GetProcessMemory());
			repub = true;
			break;
		case stop:
			ROS_INFO("Stopping...");
			command = nop;
			trajectory.header.stamp = ros::Time::now();
			trajectory.trajectory.clear();
			rrt_graph.header.stamp = ros::Time::now();
			rrt_graph.curves.clear();
			path.header.stamp = ros::Time::now();
			path.poses.clear();
			obstacles.header.stamp = ros::Time::now();
			obstacles.obstacles.clear();
			rate.sleep();
			// anhalten
			speedPublisher.publish(speed);
			state = ready;
			repub = true;
			break;
		case nop:
			if (state == following) {
				ROS_INFO_STREAM("Adding " << addNodes << " Nodes");
				markTime();
				planners[planner]->replanPath(addNodes);

				trajectory = planners[planner]->getTrajectoryMsg();
				if (DEBUG) {
					rrt_graph = planners[planner]->getRRTGraph();
				}
				path = planners[planner]->getPathMsg();
				obstacles = planners[planner]->getObstacles();

				addNodes = (1000/replanningrate)/ (timeSinceLastMark()/addNodes); // Takt in ms geteilt durch Durchlaufszeit pro Knoten
				repub = true;
			}
			break;
		}

		if (repub) {
			pathPublisher.publish(trajectory);
			if (DEBUG) {
				rrtGraphPublisher.publish(rrt_graph);
			}
			rrtPathPublisher.publish(path);
			obstaclePublisher.publish(obstacles);
			repub = false;
		}

		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}

