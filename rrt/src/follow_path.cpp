#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <fub_trajectory_msgs/Trajectory.h>
#include <tf/transform_datatypes.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Int16.h>

//generic C/C++ include
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <deque>


typedef std_msgs::Int16 speedtype ;
typedef std_msgs::UInt8 steeringtype;
//typedef std_msgs::Int16 steeringtype;


struct pos {
	pos(float a = 0, float b = 0, float c = 0) : x(a), y(b), yaw(c) {}
	float distanceTo(pos other) {
		return hypot(x - other.x, y - other.y);
	}
	float angleTo(pos other) {
		return atan2(other.y - y, other.x - x);
	}
	float x;
	float y;
	float yaw;
};

pos current;
pos startOfNextPath;

//Weg mit Zielpunkten
std::deque<pos> target_path;
std::deque<pos> next_path;

bool start = true;
ros::Time currentPathTime;

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
	//aktuelle Position und Winkel bestimmen
	tf::Pose pose;
	tf::poseMsgToTF(msg->pose.pose, pose);

	current.x = msg->pose.pose.position.x;
	current.y = msg->pose.pose.position.y;
	current.yaw = tf::getYaw(pose.getRotation());
}

void mergePaths() {

	float shortestDiff = current.distanceTo(next_path.front());
	unsigned int shortest = 0;
	for (unsigned int i = 1; i < next_path.size(); i++) {
		float nextDiff = current.distanceTo(next_path[i]);
		if (nextDiff < shortestDiff && fabs(current.angleTo(next_path[i])) < 1) {
			shortestDiff = nextDiff;
			shortest = i;
		}
	}

	while (shortest-- > 0) {
		next_path.pop_front();
	}
	target_path = next_path;
	next_path.clear();
}

void pathCallback(const fub_trajectory_msgs::Trajectory::ConstPtr& msg) {
	tf::Pose pose;
	if (currentPathTime < msg->header.stamp) {
		currentPathTime = msg->header.stamp;
		next_path.clear();
		for (fub_trajectory_msgs::TrajectoryPoint point : msg->trajectory) {
			tf::poseMsgToTF(point.pose, pose);
			next_path.push_back(
					pos(point.pose.position.x, point.pose.position.y,
							tf::getYaw(pose.getRotation())));
		}
		ROS_INFO("New Path length: %lu",next_path.size());
		mergePaths();
	}
}



void calculateSteeringAndSpeed(steeringtype& steering, speedtype& speed) {
	if (target_path.empty()) {
		steering.data = 90;
		speed.data = 0;
		return;
	}

	//Gewichte für Regler
	const float K_p = 1; //0.7
	const float K_d = 0; //0.3
	static float last_difference = 0;

	float difference = current.angleTo(target_path.front()) - current.yaw;
	if (target_path.size() > 3) {
		difference = 0.7*current.angleTo(target_path.front()) + 0.3*current.angleTo(target_path[2]) - current.yaw;
	}


	// auf/abrunden
	if (difference > M_PI) {
		difference -= 2 * M_PI;
	} else if (difference < -M_PI) {
		difference += 2 * M_PI;
	}

	float regulatedRadiens = K_p * difference + K_d * (difference - last_difference);
	float regulatedDegrees = regulatedRadiens * 180 / M_PI + 90;
	steering.data = fmax(0, fmin(180, floor(regulatedDegrees + 0.5)));

	//steering.data = 180 - steering.data;
	ROS_INFO("%f %f %f %f %f %u", current.angleTo(target_path.front()), current.yaw, difference,
			regulatedRadiens, regulatedDegrees, steering.data);

	// TODO speed abhängig vom abstand
	speed.data = 101;

}

int main(int argc, char** argv) {
	//Initialisiere ROS
	ros::init(argc, argv, "follow_path_old");

	ros::NodeHandle node;

	currentPathTime = ros::Time::now();

	ros::Rate rate(10);

	// Puffer 1, wir wollen sofortige Reaktion!
	ros::Publisher speed_pub = node.advertise<speedtype>("/manual_control/speed", 1);
	// 0 -> 180, 90 -> 270, 180 -> 360, 180+ -> 360
	// => 0-180 posten
	// Simulator:0 ist links, 180 ist rechts!
	// Auto 134: 0 ist rechts, 180 ist links
	// messungen/rechnungen: pi ist rechts, -pi ist links
	ros::Publisher steering_pub = node.advertise<steeringtype>("/steering", 1);

	speedtype speed;
	speed.data = 0;
	steeringtype steering;
	steering.data = 90;


	ros::Subscriber sub = node.subscribe("odom", 1000, odomCallback);

	//Zielpfad laden
	ros::Subscriber path_sub = node.subscribe("planned_path", 100, pathCallback);

	while (node.ok()) {
		rate.sleep();
		ros::spinOnce();

		//falls näher als 5m am nächsten Ziel
		if (!target_path.empty() && current.distanceTo(target_path.front()) < 0.8) {
			//nächsten Zielpunkt anviesieren
			target_path.pop_front();

			if (target_path.empty()) {
				ROS_INFO("Ziel erreicht");
			} else {
				ROS_INFO("Points left %lu", target_path.size());
			}
		}

		//Berechnung des zu steuernden Winkels mit PD-Regler
		calculateSteeringAndSpeed(steering, speed);

		speed_pub.publish(speed);
		steering_pub.publish(steering);

	}
	return 0;
}

