#include <ros/ros.h>

#include "wrapperlib.h"
#include "test_profiles.h"

int main(int argc, char **argv) {

	ros::init(argc, argv, "rrt_planer");
	ros::NodeHandle n;

	std::vector<std::shared_ptr<Planner>> planners = getPlanners();

	if (argc != 2) {
		ROS_INFO("Planner-Optionen:");
		int i = 0;
		for (std::shared_ptr<Planner>& planner : planners) {
			ROS_INFO(" %d: %s", i++, planner->getName());
		}

		ROS_INFO("Bitte Zielauswahl als parameter angeben von 0 bis %lu", planners.size()-1);
		return 0;
	}

	std::shared_ptr<Planner> planner = planners[atoi(argv[1])];
	ROS_INFO("Planner: %s", planner->getName());

	std::pair<Position, Position> startend = setScene(planner,EMPTY);
	markTime();
	planner->planPath(startend.first, startend.second, true);
	ROS_INFO("ZEIT: %.3f ms", timeSinceLastMark());
	ROS_INFO("MEM: %d KB", GetProcessMemory());

	ros::Publisher pathPublisher = n.advertise<fub_trajectory_msgs::Trajectory>("/planned_path", 1);
	ros::Publisher rrtGraphPublisher = n.advertise<rrt_msgs::Graph>("/rrt/graph", 1);
	ros::Publisher rrtPathPublisher = n.advertise<nav_msgs::Path>("/rrt/path", 1);
	ros::Publisher obstaclePublisher = n.advertise<rrt_msgs::Obstacles>("/rrt/obstacles", 1);

	fub_trajectory_msgs::Trajectory trajectory = planner->getTrajectoryMsg();
	nav_msgs::Path path = planner->getPathMsg();
	rrt_msgs::Graph rrt_graph = planner->getRRTGraph();
	rrt_msgs::Obstacles obstacles = planner->getObstacles();

	// 10Hz
	ros::Rate rate(10);

	int i = 0;
	while (ros::ok()) {
		pathPublisher.publish(trajectory);
		rrtGraphPublisher.publish(rrt_graph);
		rrtPathPublisher.publish(path);
		obstaclePublisher.publish(obstacles);
		ros::spinOnce();
		rate.sleep();
		if (i++ == 10) {
			markTime();
			planner.reset();
			ROS_INFO("Delete Zeit: %.3f ms", timeSinceLastMark());
			break;
		}
	}

	return 0;
}
