#ifndef SRC_EDGES_STRAIGHTEDGE_H_
#define SRC_EDGES_STRAIGHTEDGE_H_

#include "Edge.h"
#include "curves/Line.h"

class StraightEdge: public Edge {
public:
	StraightEdge(Node* startNode, Node* endNode, double distance);
	virtual ~StraightEdge();
	void setConstraints();
	static std::shared_ptr<Edge> newEdge(Node* startNode, Node* endNode, double distance);
	static EdgeInfo getEdgeInfo();
	std::vector<std::shared_ptr<Curve>> getCurves();
protected:
	std::shared_ptr<Edge> reverseEdge();
	bool checkConstraints(bool forAncestor);
	double calculateCost();
	std::shared_ptr<Line> line;
};

#endif /* SRC_EDGES_STRAIGHTEDGE_H_ */
