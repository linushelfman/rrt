class Edge;

#include "data/Node.h"
#include "curves/Curve.h"

typedef std::function<std::shared_ptr<Edge>(Node*, Node*, double)> newEdgeFunc;


#ifndef SRC_EDGES_EDGE_H_
#define SRC_EDGES_EDGE_H_
#include <cmath>
#include <vector>
#include <functional>

struct EdgeInfo {
	EdgeInfo(newEdgeFunc edgeFunc, double maxEdgeLength) {
		this->edgeFunc = edgeFunc;
		this->maxEdgeLength = maxEdgeLength;
	}
	newEdgeFunc edgeFunc;
	double maxEdgeLength;
};

class Edge : public std::enable_shared_from_this<Edge>{
public:
	Edge(); // TODO: alle edge konstruktoren protected, die Liste der curves ist ebenfalls protected!
	virtual ~Edge();
	double getCost();
	virtual void setConstraints() = 0;

	bool isInAncestorConstraints();
	bool isInConstraints();
	std::shared_ptr<Edge> getReverse();
	void setReverse(std::shared_ptr<Edge> edge);
	static std::shared_ptr<Edge> newEdge(Node* startNode, Node* endNode, double distance);
	Node* getStart();
	Node* getEnd();
	double getCostOfStart();
	double getCostOfEnd();
	void resetStartCost();

	virtual double getMaxEdgeLength();


	static constexpr double MIN_TURNING_RADIUS = 1;
	virtual std::vector<std::shared_ptr<Curve>> getCurves() = 0;

protected:
	// IDEA reverse als Wrapper implementieren! => keine extra Objekt der selben Kurve
	virtual std::shared_ptr<Edge> reverseEdge() = 0;
	virtual bool checkConstraints(bool forAncestor) = 0;
	virtual double calculateCost() = 0;
	void resetAfterMove();

	static constexpr double STD_EDGE_LENGTH = 0.3;
	std::shared_ptr<Edge> shared_from_base();

	Node* start = nullptr;
	Node* end = nullptr;
	double cost = NAN;
	double startcost = NAN;
	std::weak_ptr<Edge> reverse;
	bool inConstraints = false;
	bool constraintsChecked = false;
	bool ancestorContraintsChecked = false;
	bool inAncestorContraints = false;
};

#endif /* SRC_EDGES_EDGE_H_ */
