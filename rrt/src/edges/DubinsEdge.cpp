#include "DubinsEdge.h"
#include <ros/ros.h>

DubinsEdge::DubinsEdge(Node* startNode, Node* endNode, double distance) {
	start = startNode;
	end = endNode;
	this->distance = distance;
	calculateCurves();
}

DubinsEdge::~DubinsEdge() {
}

void DubinsEdge::setConstraints() {
	// siehe checkConstraints, wenn wir alles erreichen können, müssen wir auch nichts bewegen
	// wir bewegen den Punkt aber trotzdem näher ran
	// TODO umbauen sodass cost statt distance genutzt wird?
	if (distance > getMaxEdgeLength()) {
		std::shared_ptr<Line> line = Line::getLineFromStartEndDistance(start, end, distance);
		line->setMaxDistance(getMaxEdgeLength());
		double endyaw = end->getYaw();
		end->setPosition(line->calculateEnd());
		end->setYaw(endyaw);
		distance = getMaxEdgeLength();
		calculateCurves();
		resetAfterMove();
	}
}

std::shared_ptr<Edge> DubinsEdge::newEdge(Node* startNode, Node* endNode, double distance) {
	return std::shared_ptr<Edge>(new DubinsEdge(startNode, endNode, distance));
}

EdgeInfo DubinsEdge::getEdgeInfo() {
	return EdgeInfo(newEdge, DC_MAX_EDGE_LENGTH);
}

std::vector<std::shared_ptr<Curve> > DubinsEdge::getCurves() {
	return curves;
}

std::shared_ptr<Edge> DubinsEdge::reverseEdge() {
	return newEdge(end, start, distance);
}

bool DubinsEdge::checkConstraints(bool forAncestor) {
	// Dubins Curves können alle Punkte erreichen
	return true;
}

double DubinsEdge::calculateCost() {
	double cost = 0;
	for (std::shared_ptr<Curve>& curve : curves) {
		cost += curve->calculateCost();
	}
	return cost;
}

double DubinsEdge::getMaxEdgeLength() {
	return getEdgeInfo().maxEdgeLength;
}

void DubinsEdge::calculateCurves() {
	curves.clear();

	// alte Optionen überschreiben/löschen
	curveOptions = calculateCurves(start, end);

	// die Rückwärtsoptionen (in Reihenfole und start/endbezeichnung der Kurven) "umdrehen" und einfügen!
	for (DurbinCurveOption& option : calculateCurves(end, start)) {
		std::vector<std::shared_ptr<Curve>> newcurves;
		for (int i = option.curves.size() - 1; i >= 0; i--) {
			newcurves.push_back(option.curves[i]->getReverse());
		}
		curveOptions.push_back(DurbinCurveOption(option.type, newcurves));
		if (fabs(option.cost-curveOptions.back().cost) > 0.001) {
//			ROS_INFO_STREAM("FEHLER: typ " << option.type << " kosten " << curveOptions.back().cost << " sollte " << option.cost << " sein.");
//			ROS_INFO_STREAM("P1: " << *start << " P2 " << *end);
		}
	}

	double bestCost = 1e10;
	for (DurbinCurveOption& option : curveOptions) {
		if (option.cost < bestCost) {
			bestCost = option.cost;
			curves = option.curves;
		}
	}
	curveOptions.clear();
}

std::vector<DurbinCurveOption> DubinsEdge::calculateCurves(Node* s, Node* e) {
	std::vector<DurbinCurveOption> options;
	// unser Radius ist der minimale Radius
	double r = MIN_TURNING_RADIUS * 1.001; // zur Sicherheit gegen Rundungsprobleme

	// R1, L1, R2, L2 berechnen
	Position r1 = s->getPosWithRelDirectionAndDistance(RECHTS, r);
	Position l1 = s->getPosWithRelDirectionAndDistance(LINKS, r);
	Position r2 = e->getPosWithRelDirectionAndDistance(RECHTS, r);
	Position l2 = e->getPosWithRelDirectionAndDistance(LINKS, r);

	// 4 Abstände
	double distr1r2 = r1.distanceTo(&r2);
	double distr1l2 = r1.distanceTo(&l2);
	double distl1r2 = l1.distanceTo(&r2);
	double distl1l2 = l1.distanceTo(&l2);

	//und 4 Winkel berechnen
	double yawr1r2 = r1.angleTo(&r2);
	double yawr1l2 = r1.angleTo(&l2);
	double yawl1r2 = l1.angleTo(&r2);
	double yawl1l2 = l1.angleTo(&l2);

	// wenn d = 0 dann ist yaw undefiniert (aber auch fast egal => auf startyaw setzen damit es keine probleme gibt)
	if (distr1r2 == 0) {
		yawr1r2 = 0;
	}
	if (distr1l2 == 0) {
		yawl1r2 = 0;
	}
	if (distl1r2 == 0) {
		yawr1r2 = 0;
	}
	if (distl1l2 == 0) {
		yawl1l2 = 0;
	}

	// TODO: wie entscheiden was der beste Weg ist? evtl. mit einem anfangen der immer geht und dann andere probieren und ersetzen wenn sie besser sind
	// TODO: Problem: was ist wenn der kürzeste Weg verbaut ist, längere Wege aber möglich sind => das auch irgentwie testen bzw. den Hindernisschecker entsprechend verbessern
	// TODO evtl. direkt die Längen berechnen und nur den kürzesten erstellen?

	{   // LSL
		std::vector<std::shared_ptr<Curve>> lsl;
		Position p1 = l1.getPosOnLCircle(yawl1l2 + RECHTS, r);
		Position p2 = l2.getPosOnLCircle(yawl1l2 + RECHTS, r);

		lsl.push_back(Arc::getLArcFromCenterRadius(l1, r, s, &p1));
		lsl.push_back(Line::getLineFromStartEndDistance(&p1, &p2, distl1l2));
		lsl.push_back(Arc::getLArcFromCenterRadius(l2, r, &p2, e));
		options.push_back(DurbinCurveOption(LSL, lsl));
	}

	{   // RSR
		std::vector<std::shared_ptr<Curve>> rsr;
		Position p1 = r1.getPosOnRCircle(yawr1r2 + LINKS, r);
		Position p2 = r2.getPosOnRCircle(yawr1r2 + LINKS, r);
		rsr.push_back(Arc::getRArcFromCenterRadius(r1, r, s, &p1));
		rsr.push_back(Line::getLineFromStartEndDistance(&p1, &p2, distr1r2));
		rsr.push_back(Arc::getRArcFromCenterRadius(r2, r, &p2, e));
		options.push_back(DurbinCurveOption(RSR, rsr));
	}

	if (distl1r2 >= 2 * r) {
		// LSR
		// TODO was ist wenn = statt >?
		double alpha = acos(r / (distl1r2 / 2));
		double lineyaw = yawl1r2 - alpha;
		std::vector<std::shared_ptr<Curve>> lsr;
		Position p1 = l1.getPosOnLCircle(lineyaw, r);
		Position p2 = r2.getPosOnRCircle(lineyaw + M_PI, r);
		lsr.push_back(Arc::getLArcFromCenterRadius(l1, r, s, &p1));
		lsr.push_back(Line::getLineFromStartEnd(&p1, &p2));
		lsr.push_back(Arc::getRArcFromCenterRadius(r2, r, &p2, e));
		options.push_back(DurbinCurveOption(LSR, lsr));
	}

	if (distr1l2 >= 2 * r) {
		// RSL
		double alpha = acos(r / (distr1l2 / 2));
		double lineyaw = yawr1l2 + alpha;
		std::vector<std::shared_ptr<Curve>> rsl;
		Position p1 = r1.getPosOnRCircle(lineyaw, r);
		Position p2 = l2.getPosOnLCircle(lineyaw + M_PI, r);
		rsl.push_back(Arc::getRArcFromCenterRadius(r1, r, s, &p1));
		rsl.push_back(Line::getLineFromStartEnd(&p1, &p2));
		rsl.push_back(Arc::getLArcFromCenterRadius(l2, r, &p2, e));
		options.push_back(DurbinCurveOption(RSL, rsl));
	}

	// TODO: es gibt 2 arten von LRL/RLR => mathematisch durchrechnen welche besser ist und nur die nehmen
	// > 0.01 damit die Mittle kurve nicht leer ist, dann wäre alpha 0 und radius inf
	// bei 0.01 ist sowieso LSL/RSR besser!
	if (distl1l2 > 0.01 && distl1l2 < 4 * r) {
		// was ist wenn = statt <? => dann ist LSL/RSR sowieso besser
		// LRL
		double alpha = acos((distl1l2 / 2) / (2 * r));
		std::vector<std::shared_ptr<Curve>> lrl;
		Position p1 = l1.getPosOnLCircle(yawl1l2 - alpha, r);
		Position p2 = l2.getPosOnLCircle(yawl1l2 - M_PI + alpha, r);
		lrl.push_back(Arc::getLArcFromCenterRadius(l1, r, s, &p1));
		lrl.push_back(Arc::getArcFromStartEnd(&p1, &p2));
		lrl.push_back(Arc::getLArcFromCenterRadius(l2, r, &p2, e));
		options.push_back(DurbinCurveOption(LRL, lrl));
	}

	if (distr1r2 > 0.01 && distr1r2 < 4 * r) {
		// RLR
		double alpha = acos((distr1r2 / 2) / (2 * r));
		std::vector<std::shared_ptr<Curve>> rlr;
		Position p1 = r1.getPosOnRCircle(yawr1r2 + alpha, r);
		Position p2 = r2.getPosOnRCircle(yawr1r2 - M_PI - alpha, r);
		rlr.push_back(Arc::getRArcFromCenterRadius(r1, r, s, &p1));
		rlr.push_back(Arc::getArcFromStartEnd(&p1, &p2));
		rlr.push_back(Arc::getRArcFromCenterRadius(r2, r, &p2, e));
		options.push_back(DurbinCurveOption(RLR, rlr));
	}

	return options;
}
