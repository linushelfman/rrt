#ifndef EDGES_DUBINSEDGE_H_
#define EDGES_DUBINSEDGE_H_

#include <random>
#include "Edge.h"
#include "curves/Arc.h"
#include "curves/Line.h"

enum DurbinType {
	RSR,
	RSL,
	RLR,
	LSR,
	LSL,
	LRL
};

struct DurbinCurveOption {
	DurbinCurveOption(DurbinType type, std::vector<std::shared_ptr<Curve>> curves) {
		this->type = type;
		this->curves = curves;
		cost = 0;
		for (std::shared_ptr<Curve>& curve : curves) {
			cost += curve->calculateCost();
		}
	}
	DurbinType type;
	// TODO diese List mit list? dann muss aber überall die curves-vector zu curves-liste werden!
	std::vector<std::shared_ptr<Curve>> curves;
	double cost;
};

class DubinsEdge : public Edge {
public:
	DubinsEdge(Node* startNode, Node* endNode, double distance);
	virtual ~DubinsEdge();
	void setConstraints();
	static std::shared_ptr<Edge> newEdge(Node* startNode, Node* endNode, double distance);
	static EdgeInfo getEdgeInfo();
	std::vector<std::shared_ptr<Curve>> getCurves();
	double getMaxEdgeLength();
protected:
	std::shared_ptr<Edge> reverseEdge();
	bool checkConstraints(bool forAncestor);
	double calculateCost();
	std::vector<DurbinCurveOption> curveOptions;
	std::vector<std::shared_ptr<Curve>> curves;
	double distance;
	static constexpr double DC_MAX_EDGE_LENGTH = 2;
private:
	void calculateCurves();
	std::vector<DurbinCurveOption> calculateCurves(Node* s, Node* e);

	static constexpr double RECHTS = -M_PI / 2;
	static constexpr double LINKS = +M_PI / 2;
};

#endif /* EDGES_DUBINSEDGE_H_ */
