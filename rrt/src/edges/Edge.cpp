#include "Edge.h"
#include <ros/ros.h>


Edge::Edge() {

}

Edge::~Edge() {
	// die Kurve löscht nichts, sie wird erst beim löschen des Graphen gelöscht
}

/**
 * Prüft ob wirklich alle! Constraints stimmen, also auch winkel des Zielknoten!
 */
bool Edge::isInConstraints() {
	if (!constraintsChecked) {
		constraintsChecked = true;
		inConstraints = checkConstraints(false);
		if (inConstraints) {
			ancestorContraintsChecked = true;
			inAncestorContraints = true;
		}
	}
	return inConstraints;
}

std::shared_ptr<Edge> Edge::getReverse() {
	std::shared_ptr<Edge> reverseptr;
	if (!(reverseptr = reverse.lock())) {
		reverseptr = reverseEdge();
		reverseptr->setReverse(shared_from_this());
		reverse = reverseptr;
	}
	return reverseptr;
}

void Edge::setReverse(std::shared_ptr<Edge> edge) {
	reverse = edge;
}

Node* Edge::getStart() {
	return start;
}

Node* Edge::getEnd() {
	return end;
}

double Edge::getCostOfStart() {
	if (std::isnan(startcost)) {
		startcost = start->getCost();
	}
	return startcost;
}

void Edge::resetStartCost() {
	startcost = NAN;
}

double Edge::getCostOfEnd() {
	return getCostOfStart() + getCost();
}

std::shared_ptr<Edge> Edge::newEdge(Node* startNode, Node* endNode, double distance) {
	return nullptr;
}

/**
 * Wird verwendet um zu prüfen ob ein Knoten ein Parent sein könnte! Ignoriert Winkel des Zielknoten und abstand
 */
bool Edge::isInAncestorConstraints() {
	if (!ancestorContraintsChecked) {
		ancestorContraintsChecked = true;
		inAncestorContraints = checkConstraints(true);
	}
	return inAncestorContraints;
}

double Edge::getCost() {
	if (std::isnan(cost)) {
		cost = calculateCost();
	}
	return cost;
}

void Edge::resetAfterMove() {
	constraintsChecked = true;
	inConstraints = true;
	cost = NAN;
}

std::shared_ptr<Edge> Edge::shared_from_base() {
	// damit es keine Probleme gibt wenn Kinder shared_from_this nutzen wollen
	return shared_from_this();
}

double Edge::getMaxEdgeLength() {
	return STD_EDGE_LENGTH;
}
