#include "ros/ros.h"

#include "SteeringEdge.h"

SteeringEdge::SteeringEdge(Node* startNode, Node* endNode, double distance) {
	this->start = startNode;
	this->end = endNode;
	double yaw = start->angleTo(end);
	if (Position::angleEqual(start->getYaw(), yaw)
			|| Position::angleEqual(start->getYaw(), yaw + M_PI)) {
		linemode = true;
		line = Line::getLineFromStartEndDistance(start, end, distance);
	} else {
		arc = Arc::getArcFromStartEndDistance(start, end, distance, yaw);
		if (fabs(arc->getAlpha()) > M_PI) {
			arc->setBackwards(true);
		}
	}
}

SteeringEdge::~SteeringEdge() {

}

void SteeringEdge::setConstraints() {
	// TODO falls sich wirklich bewegt hat, dann outgoingsEdges zurücksetzen!
	if (linemode) {
		line->setMaxDistance(getMaxEdgeLength());
		end->setPosition(line->calculateEnd());
	} else {
		// TODO umbauen sodass cost statt distance genutzt wird?
		arc->setConstraints(getMaxEdgeLength(), MIN_TURNING_RADIUS);
		end->setPosition(arc->calculateEnd());
	}
	resetAfterMove();
}

std::vector<std::shared_ptr<Curve> > SteeringEdge::getCurves() {
	std::vector<std::shared_ptr<Curve>> vec;
	if (linemode) {
		vec.push_back(line);
	} else {
		vec.push_back(arc);
	}
	return vec;
}

double SteeringEdge::calculateCost() {
	if (linemode) {
		return line->calculateCost();
	} else {
		return arc->calculateCost();
	}
}

bool SteeringEdge::checkConstraints(bool forAncestor) {
	if (forAncestor) {
		// linemode => ist genau vor uns oder alpha < 45°
		return linemode || fabs(arc->getAlpha()) < M_PI / 2; // d.h.im 45° Winkel vor mir
	} else if (linemode) {
		return line->getDistance() <= getMaxEdgeLength() && Position::angleEqual(end->getYaw(), line->calculateEndYaw());
	} else {
		return arc->getDistance() <= getMaxEdgeLength() && arc->getRadius() > MIN_TURNING_RADIUS
				&& Position::angleEqual(end->getYaw(), arc->calculateEndYaw());
	}
}

EdgeInfo SteeringEdge::getEdgeInfo() {
	return EdgeInfo(newEdge, STD_EDGE_LENGTH);
}

std::shared_ptr<Edge> SteeringEdge::reverseEdge() {
	return std::shared_ptr<Edge>(
			new SteeringEdge(end, start, linemode ? line->getDistance() : arc->getDistance()));
}

std::shared_ptr<Edge> SteeringEdge::newEdge(Node* startNode, Node* endNode, double distance) {
	return std::shared_ptr<Edge>(new SteeringEdge(startNode, endNode, distance));
}
