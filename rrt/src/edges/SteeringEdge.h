#ifndef SRC_EDGES_STEERINGEDGE_H_
#define SRC_EDGES_STEERINGEDGE_H_

#include "Edge.h"
#include "curves/Arc.h"
#include "curves/Line.h"

class SteeringEdge : public Edge {
public:
	SteeringEdge(Node* startNode, Node* endNode, double distance);
	virtual ~SteeringEdge();
	void setConstraints();
	static std::shared_ptr<Edge> newEdge(Node* startNode, Node* endNode, double distance);
	static EdgeInfo getEdgeInfo();
	std::vector<std::shared_ptr<Curve>> getCurves();
protected:
	std::shared_ptr<Edge> reverseEdge();
	bool checkConstraints(bool forAncestor);
	double calculateCost();
	std::shared_ptr<Arc> arc;
	std::shared_ptr<Line> line;
	bool linemode = false;
};

#endif /* SRC_EDGES_STEERINGEDGE_H_ */
