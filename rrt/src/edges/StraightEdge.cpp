#include "StraightEdge.h"
#include "curves/Line.h"

StraightEdge::StraightEdge(Node* startNode, Node* endNode, double distance) {
	this->start = startNode;
	this->end = endNode;
	line = Line::getLineFromStartEndDistance(start,end,distance);
}

StraightEdge::~StraightEdge() {

}

void StraightEdge::setConstraints() {
	line->setMaxDistance(getMaxEdgeLength());
	end->setPosition(line->calculateEnd());
	resetAfterMove();
}

std::vector<std::shared_ptr<Curve> > StraightEdge::getCurves() {
	std::vector<std::shared_ptr<Curve>> vec;
		vec.push_back(line);
		return vec;
}

double StraightEdge::calculateCost() {
	return line->calculateCost();
}

std::shared_ptr<Edge> StraightEdge::reverseEdge() {
	return std::shared_ptr<Edge>(new StraightEdge(end, start, line->getDistance()));
}

bool StraightEdge::checkConstraints(bool forAncestor) {
	return forAncestor || line->getDistance() < getMaxEdgeLength();
}

std::shared_ptr<Edge> StraightEdge::newEdge(Node* startNode, Node* endNode, double distance) {
	return std::shared_ptr<Edge>(new StraightEdge(startNode, endNode, distance));
}

EdgeInfo StraightEdge::getEdgeInfo() {
	return EdgeInfo(newEdge, STD_EDGE_LENGTH);
}
