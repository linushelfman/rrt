#include "test_profiles.h"

std::pair<Position, Position> setScene(std::shared_ptr<Planner> planner, Profile p) {
	switch (p) {
	case EMPTY:
		return setSceneEmpty(planner);
	case PERF:
		return setScenePerf(planner);
	case LABYRINTH:
		return setSceneLabyrinth(planner);
	case OBSTACLES:
		return setSceneObstacles(planner);
	case PARKING1:
		return setSceneParking1(planner);
	case PARKING2:
		return setSceneParking2(planner);
	default:
		return setSceneEmpty(planner);
	}
}

void setSceneBorder(const std::shared_ptr<Planner>& planner, double _min=-2, double _max=18) {
	// 4 Wände im Uhrzeigersinn
	// linke Wand
	planner->addObstacleWall(_max, _min, _max, _max);
	planner->addObstacleWall(_min, _min, _max, _min);
	planner->addObstacleWall(_min, _min, _min, _max);
	planner->addObstacleWall(_min, _max, _max, _max);
}

std::pair<Position, Position> setSceneEmpty(std::shared_ptr<Planner> planner) {
//	setSceneBorder(planner, -2, 8);
	return std::make_pair(Position(0, 0, 0), Position(6, 6, 0));
}

std::pair<Position, Position> setScenePerf(std::shared_ptr<Planner> planner) {
	return std::make_pair(Position(0, 0, 0), Position(6, 6, 0));
}

std::pair<Position, Position> setSceneLabyrinth(std::shared_ptr<Planner> planner) {
	double w = 2.5;
	setSceneBorder(planner, -0.5*w, 3.5*w);

	planner->addObstacleWall(0.5*w, -0.5*w, 0.5*w, 0.5*w);
	planner->addObstacleWall(-0.5*w, 1.5*w, 1.5*w, 1.5*w);
	planner->addObstacleWall(1.5*w, 0.5*w, 1.5*w, 1.5*w);
	planner->addObstacleWall(0.5*w, 2.5*w, 2.5*w, 2.5*w);
	planner->addObstacleWall(2.5*w, 0.5*w, 2.5*w, 2.5*w);

	return std::make_pair(Position(0, 0, M_PI/4), Position(3*w, 3*w, M_PI/4));
}

std::pair<Position, Position> setSceneObstacles(std::shared_ptr<Planner> planner) {
	setSceneBorder(planner, -2, 8);
	planner->addObstaclePoint(2, 2);
	planner->addObstaclePoint(2, 4);
	planner->addObstaclePoint(3, 4);
	planner->addObstaclePoint(2.5, 3);
	planner->addObstaclePoint(5, 5);
	planner->addObstaclePoint(5, 5.5);
	planner->addObstaclePoint(5, 6);
	planner->addObstaclePoint(5.5, 5);
	planner->addObstaclePoint(6, 5);

	planner->addObstaclePoint(2, 4);
	planner->addObstaclePoint(3, 6);
	planner->addObstaclePoint(6, 1);
	planner->addObstaclePoint(5, 3);

	planner->addObstaclePoint(3, 0);
	planner->addObstaclePoint(4, 1);
	planner->addObstaclePoint(4, 3);
	planner->addObstaclePoint(5, -1);
	planner->addObstaclePoint(5, 2);

	planner->addObstaclePoint(3, 1.5);
	planner->addObstaclePoint(4, 5);
	planner->addObstaclePoint(6.5, 4);

	planner->addObstaclePoint(0, 5);
	planner->addObstaclePoint(1, 4);
	planner->addObstaclePoint(2, 2);
	planner->addObstaclePoint(0, 3);

	return std::make_pair(Position(0, 0, 0), Position(6, 6, 0));
}

std::pair<Position, Position> setSceneParking1(std::shared_ptr<Planner> planner) {
	planner->addObstacleWall(5, -1.3, 5, 0.5);
	planner->addObstacleWall(-1.5, -1.3, 5, -1.3);
	planner->addObstacleWall(-1.5, -1.3, -1.5, 0.5);
	planner->addObstacleWall(-1.5, 0.5, 5, 0.5);

	planner->addObstacleWall(-1.2, -1, 2, -1, 0.5);
	planner->addObstacleWall(3.5, -1, 4.7, -1, 0.5);

	return std::make_pair(Position(-0.5, -0.5, 0), Position(2.75, -1, 0));
}


std::pair<Position, Position> setSceneParking2(std::shared_ptr<Planner> planner) {
	planner->addObstacleWall(5, -1.3, 5, 0.5);
		planner->addObstacleWall(-1.5, -1.3, 5, -1.3);
		planner->addObstacleWall(-1.5, -1.3, -1.5, 0.5);
		planner->addObstacleWall(-1.5, 0.5, 5, 0.5);

		planner->addObstacleWall(-1.2, -1, 2.5, -1, 0.5);
		planner->addObstacleWall(3.5, -1, 4.7, -1, 0.5);

		return std::make_pair(Position(-0.5, -0.5, 0), Position(3, -1, M_PI/2));
}

