#!/usr/bin/env python
import rospy
from fub_trajectory_msgs.msg import Trajectory
from rrt_msgs.msg import Graph, Obstacles
from nav_msgs.msg import Odometry
import matplotlib.pyplot as plt
from matplotlib import style, animation, patches
from tf.transformations import euler_from_quaternion
from math import sin, cos


lastTimeStampPath = 0
lastTimeStampGraph = 0
lastTimeStampObstacles = 0
redrawneeded = False
path = [[],[],[],[]]
odom = [[],[],[],[]]
lines = []
arcs = []
obstacles = []
ax1 = 0
#um auf elemente zuzugreifen! 
#muss nicht global deklariert werden, da wir nur lesen und nicht schreiben
x = 0
y = 1
u = 2
v = 3
ARROW_LENGTH = 0.005

def path_callback(data):
  global path, redrawneeded, lastTimeStampPath

  if data.header.stamp.to_sec() > lastTimeStampPath:
    rospy.loginfo("new path!")
    lastTimeStampPath = data.header.stamp.to_sec()
    redrawneeded = True
    path = [[],[],[],[]]
    for point in data.trajectory:
      path[x].append(point.pose.position.x)
      path[y].append(point.pose.position.y)

      quaternion = (
        point.pose.orientation.x,
        point.pose.orientation.y,
        point.pose.orientation.z,
        point.pose.orientation.w)
      euler = euler_from_quaternion(quaternion)
      yaw = euler[2]
      path[u].append(cos(euler[2])*ARROW_LENGTH)
      path[v].append(sin(euler[2])*ARROW_LENGTH)

def odom_callback(data):
  global odom, redrawneeded
  new_pose = data.pose.pose;

  if odom[x] == [] or abs(new_pose.position.x - odom[x][-1]) + abs(new_pose.position.y - odom[y][-1]) > 0.1:
    redrawneeded = True
    odom[x].append(new_pose.position.x)
    odom[y].append(new_pose.position.y)

    quaternion = (
      new_pose.orientation.x,
      new_pose.orientation.y,
      new_pose.orientation.z,
      new_pose.orientation.w)
    euler = euler_from_quaternion(quaternion)
    yaw = euler[2]
    odom[u].append(cos(euler[2])*ARROW_LENGTH)
    odom[v].append(sin(euler[2])*ARROW_LENGTH)

def graph_callback(data):
  global lines, arcs, redrawneeded, lastTimeStampGraph

  if data.header.stamp.to_sec() > lastTimeStampGraph:
    rospy.loginfo("new graph!")
    lastTimeStampGraph = data.header.stamp.to_sec()
    redrawneeded = True
    lines = []
    arcs = []
    for curve in data.curves:
      if curve.type == "line":
        lines.append([curve.x,curve.y,curve.a,curve.b])
      else:
        arcs.append([curve.x,curve.y,curve.a,curve.b,curve.c])

def obstacles_callback(data):
  global obstacles, redrawneeded, lastTimeStampObstacles

  if data.header.stamp.to_sec() > lastTimeStampObstacles:
    rospy.loginfo("new obstacles!")
    lastTimeStampObstacles = data.header.stamp.to_sec()
    redrawneeded = True
    obstacles = []
    for obstacle in data.obstacles:
        obstacles.append([obstacle.x,obstacle.y,obstacle.a,obstacle.b])


def animate(i):
  global redrawneeded, ax1
  if redrawneeded:
    rospy.loginfo("redraw!")
    redrawneeded = False;

    ax1.clear()

    for obstacle in obstacles:
      rect = patches.Rectangle((obstacle[x],obstacle[y]),obstacle[u]-obstacle[x],obstacle[v]-obstacle[y], color="gray")
      ax1.add_patch(rect)
    if path[x] != []:
      ax1.plot(path[x], path[y], color="blue", linewidth=2)
    #  ax1.quiver(path[x], path[y], path[u], path[v], color="b", width=0.005)
    if odom[x] != []:
      ax1.quiver(odom[x], odom[y], odom[u], odom[v], color="r", width=0.005)
    
    for [x1,y1,x2,y2] in lines:
      ax1.plot([x1, x2], [y1, y2], color="gray", linewidth=1)

    for [x1, y1, t1, t2, r] in arcs:
      arc = patches.Arc((x1,y1),2*r,2*r, theta1=t1,theta2=t2, color="gray", linewidth=1)
      ax1.add_patch(arc)

    #https://matplotlib.org/api/_as_gen/matplotlib.patches.Arc.html
    #https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.plot.html


def listener():
  global ax1
  style.use('fivethirtyeight')

  fig = plt.figure()
  ax1 = fig.add_subplot(1,1,1)

  rospy.init_node('map', anonymous=True)
  rospy.Subscriber("/planned_path", Trajectory, path_callback)
  rospy.Subscriber("odom", Odometry, odom_callback)
  rospy.Subscriber("rrt/graph", Graph, graph_callback)
  rospy.Subscriber("rrt/obstacles", Obstacles, obstacles_callback)

  ani = animation.FuncAnimation(fig, animate, interval=1000)
  plt.show()


if __name__ == '__main__':
    listener()
