#include <ros/ros.h>

#include "wrapperlib.h"

int main(int argc, char **argv) {

	ros::init(argc, argv, "rrt_planer");

	int memoryBefore = GetProcessMemory();
	int memoryAfter = memoryBefore;

	// https://github.com/ros/ros_comm/issues/688
	ros::NodeHandle n;
	Position start = Position(0,0,0);
	Position target = Position(5,5,0);

	std::vector<std::shared_ptr<Planner>> planners = getPlanners();

	if (argc != 3) {
		int memory;
		for (std::shared_ptr<Planner>& planner : planners) {
			ROS_INFO("Planner: %s", planner->getName());

			planner->addObstaclePoint(1.5,0.5);
			planner->addObstaclePoint(2,2);
			planner->addObstaclePoint(3,2);
			planner->addObstaclePoint(1,4);

			memory = GetProcessMemory();
			markTime();
			planner->planPath(start, target, true);
			// millisekunden mit 3 Nachkommastellen
			double time = timeSinceLastMark();
			fub_trajectory_msgs::Trajectory path = planner->getTrajectoryMsg(); //für Pfadlänge, später hier analysieren!
			ROS_INFO("ZEIT: %.3f ms", time);
			ROS_INFO("SPEICHER: %d KB", GetProcessMemory() - memory);
			planner.reset();
			ROS_INFO("MEMORY-LEAK: %d KB", GetProcessMemory() - memory);
			ROS_INFO("---------------------------------------");
		}

		memoryAfter = GetProcessMemory();

		ROS_INFO("Speicher vorher/nachher/differenz: %d/%d/%d KB", memoryBefore, memoryAfter,
				memoryAfter - memoryBefore);

	} else if (argc == 3 && strcmp(argv[1], "mem") == 0) {
		// Memory test
		// die Tests davor sind relativ, da er manche sachen erstmal in physikalischen Speicher laden muss
		// => erst ab 2. durchlauf sind die Ergebnisse sinnvoll
		int memmoryStart = GetProcessMemory();
		std::shared_ptr<Planner> planner = planners[atoi(argv[2])];
		planners.clear();
		ROS_INFO("Memorytest für %s", planner->getName());

		// einmal planen, damit Vergleich stimmt
		planner->planPath(start, target, true);
		memoryBefore = GetProcessMemory();
		for (unsigned int i = 0; i < 100; i++) {
			planner->planPath(start, target, true);
			if (i % 10 == 9) {
				memoryAfter = GetProcessMemory();
				ROS_INFO("Durchläufe %d bis %d: Speicher vorher/nachher/differenz: %d/%d/%d KB",
						i - 10, i, memoryBefore, memoryAfter, memoryAfter - memoryBefore);
				memoryBefore = memoryAfter;
			}
		}
		sleep(1);
		memoryAfter = GetProcessMemory();

		ROS_INFO("Speicher vorher/nachher/differenz: %d/%d/%d KB", memmoryStart, memoryAfter,
				memoryAfter - memmoryStart);
	} else {
		ROS_INFO("für erweiterten Memorytest mit argument 'mem x' aufrufen, wobei x die nummer des RRT ist");
	}

	return 0;
}
