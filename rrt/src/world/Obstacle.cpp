#include "Obstacle.h"

Obstacle::Obstacle(std::string type, double x, double y, double a, double b, double c) {
	this->type = type;
	this->x = x;
	this->y = y;
	this->a = a;
	this->b = b;
	this->c = c;
}

Obstacle::~Obstacle() {

}

rrt_msgs::Obstacle Obstacle::toMsg() {
	rrt_msgs::Obstacle obstacle;
	obstacle.type = type;
	obstacle.x = x;
	obstacle.y = y;
	obstacle.a = a;
	obstacle.b = b;
	obstacle.c = c;
	return obstacle;
}

bool Obstacle::hasCollision(Position& position) {
	return checkPos(position.getX(), position.getY());
}

Obstacle* Obstacle::newRectangle(double x, double y, double xdiff, double ydiff) {
	return new Obstacle("rectangle", x, y, x + fabs(xdiff), y + fabs(ydiff), 0);
}

bool Obstacle::checkPos(double posx, double posy) {
	return x <= posx && posx <= a && y <= posy && posy <= b;
}

double Obstacle::getX() {
	return x;
}

double Obstacle::getY() {
	return y;
}

double Obstacle::getA() {
	return a;
}

double Obstacle::getB() {
	return b;
}
