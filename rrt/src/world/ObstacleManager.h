class ObstacleManager;

#ifndef SRC_OBSTACLEMANAGER_H_
#define SRC_OBSTACLEMANAGER_H_
#include <ros/ros.h>
#include <rrt_msgs/Obstacles.h>
#include "Obstacle.h"
#include "data/Position.h"
#include "curves/Curve.h"
#include <vector>

/**
 * 0. Subscriber + Publisher zum empfangen von Scan und senden von /rrt/obstacles
 * 0.5 Karte erweitern um /scan und /rrt/obstacles, dabei als param angeben können, welche Elemente gezeichnet werden
 * 1. Checken ob ein Knoten mit einem Hinderniss kollidiert, wenn der Knoten erstellt wird
 * 2. Checken ob Knoten mit einem Hinderniss kollidieren, wenn das Hinderniss erstellt wird
 *    a) alle Knoten + alle Nachfolger als zu löschen markieren
 *    b) alle direkt zu löschenden Knoten löschen
 *    c) die Nachfolger der direkt zu löschenden Knoten rekursiv löschen,
 *       jeweils prüfen ob man ihnen ein neues Elternteil geben kann, was nicht gelöscht werden soll
 *       => teile erhalten, aber prüfen das man keinen Kreis baut
 */

class ObstacleManager {
public:
	ObstacleManager();
	virtual ~ObstacleManager();
	void addObstaclePoint(double x, double y, double radius = 0.2);
	void addObstacle(double x1, double y1, double x2, double y2);
	bool hasCollision(Position& position);
	bool hasCollision(const std::vector<std::shared_ptr<Curve>>& curves);
	bool hasCollision(double x, double y);
	rrt_msgs::Obstacles getObstaclesMsg();
private:
	std::pair<int,int> indexOfNode(Position* node);
	std::pair<int,int> indexOfPos(double x, double y);
	int indexOfDouble(double xy);
	void addObstacle(Obstacle* obstacle);

	std::vector<std::unique_ptr<Obstacle>> obstacles;
	constexpr static double MAX_DISTANCE_BETWEEN_CURVE_POINTS = 0.05;

	const double SPACING = 0.05;
	std::map<std::pair<int,int>, bool> collisionMap;
};

#endif /* SRC_OBSTACLEMANAGER_H_ */
