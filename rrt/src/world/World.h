#ifndef SRC_WORLD_H_
#define SRC_WORLD_H_

#include "data/Node.h"
#include <random>

class World {
public:
	World();
	virtual ~World();
	double getMinX();
	double getMaxX();
	double getMinY();
	double getMaxY();
	void setEmptySimWorld();
	void setSimMap();
	void setFUMap();
	bool isInBoundary(Node* node);
	Position sample();

	void setBoundary(Position start, Position end, double border = 2.0);
	void setBoundarys(double minX, double maxX, double minY, double maxY);
private:
	double minX;
	double maxX;
	double minY;
	double maxY;
	double minYaw = -M_PI;
	double maxYaw = M_PI; // -Pi bis Pi
	std::random_device generator;
	std::function<double()> randomX;
	std::function<double()> randomY;
	std::function<double()> randomYaw;
};

#endif /* SRC_WORLD_H_ */
