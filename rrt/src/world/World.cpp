#include "World.h"

World::World() {
	// Start ist 0;0 mit yaw=0
	// man schaut in richtung pos. x-Achse, y-Achse ist nach links
	setEmptySimWorld();
}

World::~World() {
	// keine dynamischen Daten
}

void World::setBoundarys(double minX, double maxX, double minY, double maxY) {
	this->minX = minX;
	this->maxX = maxX;
	this->minY = minY;
	this->maxY = maxY;

	std::uniform_real_distribution<double> distributionX(minX, maxX);
	std::uniform_real_distribution<double> distributionY(minY, maxY);
	std::uniform_real_distribution<double> distributionYaw(minYaw, maxYaw);

	randomX = std::bind(distributionX, std::ref(generator));
	randomY = std::bind(distributionY, std::ref(generator));
	randomYaw = std::bind(distributionYaw, std::ref(generator));
}

void World::setEmptySimWorld() {
	setBoundarys(-10, 10, -10, 10);
}

void World::setSimMap() {
	// Sim-Map: x=[-6;6] y=[-11;1], mit Wänden!
	setBoundarys(-6, 6, -11, 1);
}

void World::setFUMap() {
	// Map: x=[0;6] y=[0;6], ohne Wände!
	setBoundarys(0, 6, 0, 6);
}

double World::getMinX() {
	return minX;
}

double World::getMaxX() {
	return maxX;
}

double World::getMinY() {
	return minY;
}

double World::getMaxY() {
	return maxY;
}

bool World::isInBoundary(Node* node) {
	return minX <= node->getX() && node->getX() <= maxX && minY <= node->getY()
			&& node->getY() <= maxY;
}

void World::setBoundary(Position start, Position end, double border) {
	setBoundarys(std::min(start.getX(), end.getX()) - border,
			std::max(start.getX(), end.getX()) + border,
			std::min(start.getY(), end.getY()) - border,
			std::max(start.getY(), end.getY()) + border);
}

Position World::sample() {
	return Position(randomX(), randomY(), randomYaw());
}
