#ifndef SRC_OBSTACLE_H_
#define SRC_OBSTACLE_H_
#include <rrt_msgs/Obstacle.h>
#include <string>
#include "data/Node.h"

/*
 * Standardhinderniss ist erstmal ein Rechteck.
 * x,y ist sind die kleineren x/y-Werte, a,b die gegenüberliegende Ecke
 * type = rectangle
 *
 */
class Obstacle {
public:
	Obstacle(std::string type, double x, double y, double a, double b, double c);
	virtual ~Obstacle();
	rrt_msgs::Obstacle toMsg();
	bool hasCollision(Position& position);
	static Obstacle* newRectangle(double x, double y, double xdiff, double ydiff);
	double getX();
	double getY();
	double getA();
	double getB();
private:
	bool checkPos(double posx, double posy);
	std::string type;
	double x;
	double y;
	double a;
	double b;
	double c;
};

#endif /* SRC_OBSTACLE_H_ */
