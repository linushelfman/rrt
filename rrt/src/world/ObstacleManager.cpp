#include "ObstacleManager.h"

ObstacleManager::ObstacleManager() {

}

ObstacleManager::~ObstacleManager() {

}

bool ObstacleManager::hasCollision(Position& position) {
	return hasCollision(position.getX(),position.getY());
}

void ObstacleManager::addObstaclePoint(double x, double y, double radius) {
	addObstacle(Obstacle::newRectangle(x - radius, y - radius, 2 * radius, 2 * radius));
}

void ObstacleManager::addObstacle(double x1, double y1, double x2, double y2) {
	addObstacle(
			new Obstacle("rectangle", std::min<double>(x1, x2), std::min<double>(y1, y2),
					std::max<double>(x1, x2), std::max<double>(y1, y2), 0));
}

bool ObstacleManager::hasCollision(const std::vector<std::shared_ptr<Curve>>& curves) {
	for (const std::shared_ptr<Curve>& curve : curves) {
		for (Position& pos : curve->calculatePointsOnCurve(MAX_DISTANCE_BETWEEN_CURVE_POINTS)) {
			if (hasCollision(pos)) {
				return true;
			}
		}
	}
	return false;
}

rrt_msgs::Obstacles ObstacleManager::getObstaclesMsg() {

	rrt_msgs::Obstacles obstaclesMsg;

	for (std::unique_ptr<Obstacle>& obstacle : obstacles) {
		obstaclesMsg.obstacles.push_back(obstacle->toMsg());
	}

	obstaclesMsg.header.frame_id = "map";
	obstaclesMsg.header.stamp = ros::Time::now();

	return obstaclesMsg;
}

void ObstacleManager::addObstacle(Obstacle* obstacle) {
	int xmin = indexOfDouble(obstacle->getX());
	int xmax = indexOfDouble(obstacle->getA());
	int ymin = indexOfDouble(obstacle->getY());
	int ymax = indexOfDouble(obstacle->getB());
	std::pair<int, int> index = std::pair<int, int>(xmin, ymin);
	for (index.first = xmin; index.first <= xmax; index.first++) {
		for (index.second = ymin; index.second <= ymax; index.second++) {
			collisionMap[index] = true;
		}
	}
	obstacles.push_back(std::unique_ptr<Obstacle>(obstacle));
}

std::pair<int, int> ObstacleManager::indexOfNode(Position* node) {
	return indexOfPos(node->getX(), node->getY());
}

int ObstacleManager::indexOfDouble(double xy) {
	// floor, damit negative zahlen auch nach unten gerundet werden
	return floor(xy / SPACING);
}

std::pair<int, int> ObstacleManager::indexOfPos(double x, double y) {
	return std::pair<int, int>(indexOfDouble(x), indexOfDouble(y));
}

bool ObstacleManager::hasCollision(double x, double y) {
	return collisionMap.find(std::pair<int, int>(floor(x / SPACING), floor(y / SPACING)))
			!= collisionMap.end();
}
