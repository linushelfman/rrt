#include "Car.h"
#include <tf/tf.h>

Car::Car() {
	ros::NodeHandle node;

	odomSubscriber = node.subscribe("/odom", 1000, &Car::odomCallback, this);

	currentPosition = Position(0,0,0);
}

Car::~Car() {
	odomSubscriber.shutdown();
}

void Car::odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
	double y = msg->pose.pose.position.y;
	double x = msg->pose.pose.position.x;

	tf::Pose pose;
	tf::poseMsgToTF(msg->pose.pose, pose);
	double yaw = tf::getYaw(pose.getRotation());

	currentPosition.setPosition(x, y, yaw);
	ROS_DEBUG_STREAM("Auto-Position: " << currentPosition);
}

Position Car::getPosition() {
	return currentPosition;
}
