#ifndef SRC_CAR_H_
#define SRC_CAR_H_

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include "data/Position.h"

/**
 * als Basisklasse gedacht, muss evtl. irgentwas für Model und virtuelles auto aufgespalten werden
 */
class Car {
public:
	Car();
	virtual ~Car();
	Position getPosition();
	// IDEA beschränkungen und Eigenschaften des Fahrzeugs
private:
	void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);
	ros::Subscriber odomSubscriber;
	Position currentPosition;
};

#endif /* SRC_CAR_H_ */
