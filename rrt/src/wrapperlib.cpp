#include "wrapperlib.h"

// Libs für GetProcessMemory
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys/types.h"
#include "sys/sysinfo.h"

// Libs für getPlanners
#include "rrt/RRTFactory.h"

// für getTime
#include <chrono>

int parseLine(char *line) {
	// This assumes that a digit will be found and the line ends in " Kb".
	int i = strlen(line);
	const char *p = line;
	while (*p < '0' || *p > '9') p++;
	line[i - 3] = '\0';
	i = atoi(p);
	return i;
}

int GetProcessMemory() {
	FILE *file = fopen("/proc/self/status", "r");
	char line[128];

	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "VmRSS:", 6) == 0) {
			fclose(file);
			return parseLine(line);
		}
	}
	fclose(file);
	return 0;
}

std::vector<std::shared_ptr<Planner>> getPlanners() {
	return {
		std::shared_ptr<Planner>(new RRTFactory("RRT",false, false, false)),
		std::shared_ptr<Planner>(new RRTFactory("RRT* ohne Cache",true, false, false)),
		std::shared_ptr<Planner>(new RRTFactory("RRT* mit Cache",true, true, false)),
		std::shared_ptr<Planner>(new RRTFactory("RRT* Informed",true, true, true)),
		std::shared_ptr<Planner>(new RRTFactory("RRT mit SteeringEdge",false, false, false, SteeringEdge::getEdgeInfo())),
		std::shared_ptr<Planner>(new RRTFactory("RRT* mit SteeringEdge",true, true, false, SteeringEdge::getEdgeInfo())),
		RRTFactory::getRRTForTests(true,false,false,true),
		RRTFactory::getRRTForTests(true,true,true,true),
		RRTFactory::getRRTForTests(true,true,true,false),
		std::shared_ptr<Planner>(new RRTFactory("RRT mit DurbinEdge",false, false, false, DubinsEdge::getEdgeInfo())),
		std::shared_ptr<Planner>(new RRTFactory("RRT* mit DurbinEdge",true, true, false, DubinsEdge::getEdgeInfo())),
	};
}

typedef std::chrono::steady_clock myclock;

myclock::time_point start;

void markTime() {
	start = myclock::now();
}

double timeSinceLastMark() {
	return std::chrono::duration_cast<std::chrono::microseconds>(myclock::now() - start).count() / 1000.0;
}

