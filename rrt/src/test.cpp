#include <ros/ros.h>

#include "wrapperlib.h"
#include "rrt/RRTFactory.h"
#include "curves/Arc.h"
#include "edges/DubinsEdge.h"
#include <cmath>
#include "data/BoxGraph.h"
#include "test_profiles.h"

typedef std::function<std::shared_ptr<Planner>()> plannerGen;

void testCurves() {
	ros::NodeHandle n;
	ros::Publisher rrtGraphPublisher = n.advertise<rrt_msgs::Graph>("/rrt/graph", 1);
	ros::Publisher pathPublisher = n.advertise<fub_trajectory_msgs::Trajectory>("/planned_path", 1);
	std::vector<std::shared_ptr<Curve> > curves;
	fub_trajectory_msgs::Trajectory path;
	path.header.frame_id = "map";
	path.header.stamp = ros::Time::now();
	path.child_frame_id = "base_link";
	rrt_msgs::Graph new_graph;
	new_graph.header.frame_id = "map";
	new_graph.header.stamp = ros::Time::now();
	// ------------- Test --------------------------
	Position basepos = Position(0, 0, 0);
	Position testpos = Position(4.222, 4.629, 0.891);
	Node* base = new Node(basepos, nullptr, 0, Edge::newEdge);
	Node* testNode = new Node(testpos, base, testpos.distanceTo(base), DubinsEdge::newEdge);
	ROS_INFO_STREAM("KOSTEN " << testNode->getCost());
	curves = testNode->getEdge()->getCurves();
	//	curves.push_back(
	//			Arc::getArcFromCenterRadius(Position(), 1, new Position(1, 0, M_PI / 2),
	//					new Position(0, 1, -M_PI), true));
	//	curves.push_back(Arc::getArcFromStartEnd(new Position(1, 0, -M_PI / 2), new Position(0, 1, 0)));
	//	curves.push_back(Arc::getArcFromStartEnd( new Position(0, 1, -M_PI), new Position(1, 0, M_PI / 2)));
	// ------------- Nachbereitung ------------------
	double length = 0.4;
	for (std::shared_ptr<Curve> curve : curves) {
		new_graph.curves.push_back(curve->getMsg());
		//		ROS_INFO_STREAM("curve " << curve->calculateCost());
		fub_trajectory_msgs::TrajectoryPoint next_point;
		next_point.pose.position = curve->getStart().getPositionMsg();
		next_point.pose.orientation = curve->getStart().getOrientationMsg();
		//		ROS_INFO_STREAM("Start " << curve->getStart());
		path.trajectory.push_back(next_point);
		for (Position pos : curve->calculatePointsOnCurve(length)) {
			fub_trajectory_msgs::TrajectoryPoint next_point;
			next_point.pose.position = pos.getPositionMsg();
			next_point.pose.orientation = pos.getOrientationMsg();
			//			ROS_INFO_STREAM("Pos " << pos);
			path.trajectory.push_back(next_point);
		}
		//		ROS_INFO_STREAM("Ende " << curve->calculateEnd());
	}
	// 10Hz
	ros::Rate rate(10);
	int i = 0;
	while (ros::ok()) {
		rrtGraphPublisher.publish(new_graph);
		pathPublisher.publish(path);
		ros::spinOnce();
		rate.sleep();
		if (i++ == 10) {
			markTime();
			break;
		}
	}
}

plannerGen getPlannerGen(bool rewire = true, bool cache = false, double k = 1.0, double r = 0.3) {

	std::string name = "RRT";
	if (rewire) {
		name.append("*");
	}
	if (cache) {
		name.append("C");
	}
	char tmp[20];
	graphGen graph;
	if (k < 0) {
		name.append(" Vec");
		graph = VectorGraph::getGraphGen();
	} else {
		name.append(" Box");
		sprintf(tmp, "%0.1f", k);
		name.append(tmp);
		graph = BoxGraph::getGraphGen(k);
	}
	sprintf(tmp, " r=%0.1f", r);
	name.append(tmp);
	EdgeInfo edge = EdgeInfo(StraightEdge::newEdge, r);
	return [name, rewire, cache, graph, edge]() {return std::shared_ptr<Planner>(new RRTFactory(name, rewire, cache, false, edge, graph));};
}

plannerGen getPlannerGenForTest(bool hybrid, bool informed, bool smart, bool bestAncestor) {
	return [hybrid,informed,smart,bestAncestor]() {return RRTFactory::getRRTForTests(hybrid,informed,smart,bestAncestor);};
}


void perfPlanner(plannerGen getPlanner, int n, int nodes, Profile profile) {

	bool fixedTarget = true;

	double timesum = 0, timemin = 1e10, timemax = 0;
	int memsum = 0, memmin = 10000000, memmax = 0;
	double costsum = 0;
	double found = 0;

	int memoryBefore = GetProcessMemory();
	ROS_INFO("Speicher vorher: %d KB", memoryBefore);
	// 10 durchläufe zum messen
	for (int i = 1; i <= n; i++) {
		std::shared_ptr<Planner> planner = getPlanner();
		std::pair<Position, Position> startend = setScene(planner, profile);
		markTime();
		planner->planPath(startend.first, startend.second, fixedTarget, nodes);
		double time = timeSinceLastMark();
		timesum += time;
		timemin = std::min(time, timemin);
		timemax = std::max(time, timemax);
		int mem = GetProcessMemory();
		memsum += mem;
		memmin = std::min(mem, memmin);
		memmax = std::max(mem, memmax);
		double cost = planner->getCost();
		if (cost > 1) {
			found += 1;
			costsum += cost;
		}

		ROS_INFO("Durchlauf/Zeit ms/Strecke m/Speicher KB: %d; %0.3f; %0.2f; %d", i, time, cost, mem);
	}
//	ROS_INFO("RESULT: Name/Lauefe/Knoten/Zeit (min/max/avg)/Mem (min/max/avg): %s; %d; %d; %0.3f; %0.3f; %0.3f; %d; %d; %d;",
//			getPlanner()->getName(), n, nodes, timemin, timemax, timesum / n, memmin, memmax, memsum / n);
	ROS_INFO("RESULT-ALL: %d %s; %d; %0.2f; %0.2f; %0.3f; %d;", profile, getPlanner()->getName(), nodes,
			found / n, costsum / found, timesum / n, memsum / n);
	ROS_INFO("RESULT-ZEIT: %d %s; (%d, %0.2f)", profile, getPlanner()->getName(), nodes, timesum / n);
	ROS_INFO("RESULT-STRECKE: %d %s; (%d, %0.2f)", profile, getPlanner()->getName(), nodes, costsum / found);
}

void testBox(bool rewire = true, bool cache = false, int n = 10, int nodes = 10000,
		double r = 0.3) {
	ROS_INFO_STREAM("RESULT: RESULT-SHORT: ============================================== ");
	std::vector<double> listk = { -1, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3,
			1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 10 };
//	planners.reverse(); // Achtung: in eine Richtung wird es besser, in die andere schlechter => beide messen und mittelwert nehmen!
	for (double k : listk) {
		perfPlanner(getPlannerGen(rewire, cache, k, r), n, nodes, PERF);
	}
}

void testBoxShort(bool rewire = true, bool cache = false, int n = 10, int nodes = 10000,
		double r = 0.3) {
	ROS_INFO_STREAM("RESULT: RESULT-SHORT: ============================================== ");
	std::vector<double> listk = { -1, 0.3, 0.6, 0.9, 1.2, 1.5, 2, 3, 10 };
//	planners.reverse(); // Achtung: in eine Richtung wird es besser, in die andere schlechter => beide messen und mittelwert nehmen!
	for (double k : listk) {
		perfPlanner(getPlannerGen(rewire, cache, k, r), n, nodes, PERF);
	}
}


void testPerf(int n = 10, int nodes = 10000, double r = 0.3) {
	ROS_INFO_STREAM("RESULT: RESULT-SHORT: =============================================== ");

	// (RRT/RRT*) x (Cache an/aus) x  (Vektor, Box2) = 8 // RRT + Cache macht keinen Sinn! => 6
	std::vector<plannerGen> planners = {
		getPlannerGen(false, false, -1.0, r),
		getPlannerGen(false, false, 1.0, r),
		getPlannerGen(true, false, -1.0, r),
		getPlannerGen(true, false, 1.0, r),
		getPlannerGen(true, true, -1.0, r),
		getPlannerGen(true, true, 1.0, r) };

	for (plannerGen getPlanner : planners) {
		perfPlanner(getPlanner, n, nodes, PERF);
	}
}

void testAllPerf(int n = 5) {
	// 7-8min auf PC!
	// k für RRT testen
	testBox(false, false, n, 5000);
	// k für RRT* noCache testen
	testBox(true, false, n, 5000);
	// k für RRT* Cache testen
	testBox(true, true, n, 5000);
	// k für Nodes testen:
//	testBoxShort(true, false, n, 1000, 0.3);
//	testBoxShort(true, false, n, 2000, 0.3);
//	testBoxShort(true, false, n, 5000, 0.3);
//	testBoxShort(true, false, n, 10000, 0.3);
	// k für r testen
//	testBoxShort(true, false, n, 1000, 0.3);
//	testBoxShort(true, false, n, 1000, 0.5);
//	testBoxShort(true, false, n, 1000, 0.7);
//	testBoxShort(true, false, n, 1000, 1.0);
	// perf für 1k, 2k, 5k, 10k, 20k testen
	testPerf(n, 1000);
	testPerf(n, 2000);
	testPerf(n, 5000);
	testPerf(n, 10000);
	testPerf(n, 20000);
}

void testProfilesForRRTAlgorithms(int n = 5) {
	// RRT* vs RRT* Hybrid vs RRT* Informed Hybrid vs RRT* Smart Hybrid vs RTT* BestAncestor Hybrid vs RRT* Smart Informed Hybrid vs RRT* Smart Informed Hybrid BestAncestor
	// x 1000/2000/3000/4000/5000-Knoten?
	// nur mit BoxGraph Cache

	std::vector<plannerGen> planners = {
			getPlannerGenForTest(false, false, false, false),
			getPlannerGenForTest(true, false, false, false),
			getPlannerGenForTest(true, true, false,	false),
			getPlannerGenForTest(true, false, true, false),
			getPlannerGenForTest(true, true, true, false),
			getPlannerGenForTest(true, false, false, true),
//			getPlannerGenForTest(true, true, true, true) wirft zu häufig segfault
			};
	std::vector<Profile> profiles = { EMPTY, LABYRINTH, OBSTACLES };
	std::vector<int> nodeslist = { 1000, 2000, 3000, 4000, 5000 };

	for (plannerGen getPlanner : planners) {
		for (Profile p : profiles) {
			for (int nodes : nodeslist) {
				perfPlanner(getPlanner, n, nodes, p);
			}
		}
	}
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "rrt_planer");
	ros::NodeHandle node;

	testAllPerf(10);
//	ROS_INFO("RESULT ======================================");
	testProfilesForRRTAlgorithms(10);

//	testCurves();

	return 0;
}
