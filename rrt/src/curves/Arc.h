#ifndef SRC_CURVES_ARC_H_
#define SRC_CURVES_ARC_H_

#include "Curve.h"
#include <random>

class Arc: public Curve {
public:
	// yaw ist der Winkel zwischen start und ende, startyaw der Winkel vom start, startyaw-yaw ist damit die abweichung
	Arc(Position start, double distance, double yaw, double startyaw);
	Arc(Position center, double radius, double distance, double yaw, double startyaw, double theta1, double theta2, bool counterclockwise);
	Arc(Position center, double radius, double distance, double yaw, double startyaw, double theta1, double theta2, bool counterclockwise, bool backwards);
	virtual ~Arc();
	double calculateCost();
	void setConstraints(double maxDistance, double minRadius);
	Position calculateEnd();
	double calculateEndYaw();
	std::vector<Position> calculatePointsOnCurve(double maxDistance);
	double getAlpha();
	double getRadius();
	void setBackwards(bool backwards);
	bool isBackwards();
	Position getStart();
	std::shared_ptr<Curve> getReverse();
	static std::shared_ptr<Arc> getArcFromStartEndDistance(Position* start, Position* end, double distance);
	static std::shared_ptr<Arc> getArcFromStartEndDistance(Position* start, Position* end, double distance, double yaw);
	static std::shared_ptr<Arc> getArcFromStartEnd(Position* start, Position* end);
	static std::shared_ptr<Arc> getArcFromCenterRadius(Position center, double radius, Position* start, Position* end, bool counterclockwise);
	static std::shared_ptr<Arc> getLArcFromCenterRadius(Position center, double radius, Position* start, Position* end);
	static std::shared_ptr<Arc> getRArcFromCenterRadius(Position center, double radius, Position* start, Position* end);
	rrt_msgs::Curve getMsg();

protected:
	double startyaw;
	double alpha;



	double theta1;
	double theta2;
	double radius;

	bool backwards = false;
	bool counterclockwise; // d.h. Mathematischer drehsinn bzw. die richtung in die yaw steigt

private:
	double deg(double rad);
	void calcAlphaAndRadius();
	bool isCounterclockwiseForDrawing();
	double getalphafordrawing();
};

#endif /* SRC_CURVES_ARC_H_ */
