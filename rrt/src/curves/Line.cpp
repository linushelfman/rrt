#include "Line.h"
#include "data/Node.h"

Line::Line(Position start, double x, double y, double distance, double yaw) :
		Curve(distance, yaw) {
	this->x = x;
	this->y = y;
	reference = start;
	if (Node::angleEqual(start.getYaw(), yaw + M_PI)) {
		reverse = true;
	}
}

Line::~Line() {

}

void Line::setMaxDistance(double maxDistance) {
	if (distance > maxDistance) {
		interpolateTo(maxDistance);
	}
}

std::shared_ptr<Line> Line::getLineFromStartEndDistanceYaw(Position* start, Position* end,
		double distance, double yaw) {
	return std::make_shared<Line>(start->getPosition(), end->getX() - start->getX(),
			end->getY() - start->getY(), distance, yaw);
}

std::shared_ptr<Line> Line::getLineFromStartEndDistance(Position* start, Position* end,
		double distance) {
	return getLineFromStartEndDistanceYaw(start, end, start->distanceTo(end),
			Position::getAngle(start, end));
}

std::shared_ptr<Line> Line::getLineFromStartEnd(Position* start, Position* end) {
	return getLineFromStartEndDistance(start, end, start->distanceTo(end));
}

rrt_msgs::Curve Line::getMsg() {
	rrt_msgs::Curve curvemsg;
	curvemsg.type = "line";
	curvemsg.x = reference.getX();
	curvemsg.y = reference.getY();
	curvemsg.a = reference.getX() + x;
	curvemsg.b = reference.getY() + y;
	return curvemsg;
}

Position Line::calculateEnd() {
	return Position(reference.getX() + x, reference.getY() + y, calculateEndYaw());
}

double Line::calculateEndYaw() {
	return reference.getYaw();
}

std::vector<Position> Line::calculatePointsOnCurve(double maxDistance) {
	std::vector<Position> points;
	int n = distance / maxDistance + 1; // entspricht abrunden und min 1, bloß falls distance ein vielfaches von maxDistance ist, wird einer zu viel geteilt, das ist aber ok
	// wir runden auf, aber min. 1 damit kein div0, d.h. wenn < maxD wird nur Ende geprüft (+anfang an anderer Stelle)
	// wenn > maxD wird zweimal geprüft, d.h. minD ist damit distance oder maxD/2
	double xdiff = x / n;
	double ydiff = y / n;
	double posx = reference.getX();
	double posy = reference.getY();
	// von Anfang nach Ende zählen aber start auslassen dadurch das wir erst diff addieren
	// da n = die anzahl der Teile, genau n-mal prüfen
	for (int i = 0; i < n; i++) {
		posx += xdiff;
		posy += ydiff;
		points.push_back(Position(posx, posy, reference.getYaw()));
	}
	return points;
}

void Line::interpolateTo(double distance) {
	this->distance = distance;
	x = getXFromDistanceYaw(distance,yaw);
	y = getYFromDistanceYaw(distance,yaw);
}

std::shared_ptr<Curve> Line::getReverse() {
	return std::make_shared<Line>(calculateEnd(), -x, -y, distance, Node::normAngle(yaw + M_PI));
}

void Line::rotateTo(double yaw) {
	this->yaw = Node::normAngle(yaw);
	x = getXFromDistanceYaw(distance,this->yaw);
	y = getYFromDistanceYaw(distance,this->yaw);
}

Position Line::getStart() {
	return reference;
}
