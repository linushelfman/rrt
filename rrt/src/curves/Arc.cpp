#include "Arc.h"
#include <cmath>
#include <data/Node.h>
#include <ros/ros.h>

Arc::Arc(Position start, double distance, double yaw, double startyaw) :
		Curve(distance, yaw) {
	this->startyaw = startyaw;

	reference = start;
	radius = 0;
	calcAlphaAndRadius();
}

Arc::Arc(Position center, double radius, double distance, double yaw, double startyaw,
		double theta1, double theta2, bool counterclockwise) :
		Curve(distance, yaw) {
	this->startyaw = startyaw;
	reference = center;
	this->radius = radius;
	this->theta1 = theta1;
	this->theta2 = theta2;
	this->alpha = theta2 - theta1;
	this->counterclockwise = counterclockwise;
	if (counterclockwise && alpha < 0) {
		alpha += 2*M_PI;
	} else if (!counterclockwise && alpha > 0) {
		alpha -= 2*M_PI;
	}
}

Arc::Arc(Position center, double radius, double distance, double yaw, double startyaw,
		double theta1, double theta2, bool counterclockwise, bool backwards) :
		Curve(distance, yaw) {
	this->startyaw = startyaw;
	reference = center;
	this->radius = radius;
	this->theta1 = theta1;
	this->theta2 = theta2;
	this->alpha = theta2 - theta1;
	this->backwards = backwards;
	this->counterclockwise = counterclockwise;
	if (counterclockwise && alpha < 0) {
		alpha += 2*M_PI;
	} else if (!counterclockwise && alpha > 0) {
		alpha -= 2*M_PI;
	}
}

std::shared_ptr<Arc> Arc::getArcFromCenterRadius(Position center, double radius, Position* start,
		Position* end, bool counterclockwise) {
	return std::make_shared<Arc>(center, radius, start->distanceTo(end), start->angleTo(end),
			start->getYaw(), center.angleTo(start), center.angleTo(end), counterclockwise);
}

std::shared_ptr<Arc> Arc::getLArcFromCenterRadius(Position center, double radius, Position* start,
		Position* end) {
	return getArcFromCenterRadius(center, radius, start, end, true);
}

std::shared_ptr<Arc> Arc::getRArcFromCenterRadius(Position center, double radius, Position* start,
		Position* end) {
	return getArcFromCenterRadius(center, radius, start, end, false);
}

std::shared_ptr<Arc> Arc::getArcFromStartEndDistance(Position* start, Position* end,
		double distance) {
	return getArcFromStartEndDistance(start, end, distance,
			Node::getAngle(start, end));
}

std::shared_ptr<Arc> Arc::getArcFromStartEndDistance(Position* start, Position* end,
		double distance, double yaw) {
	return std::make_shared<Arc>(start->getPosition(), distance, yaw, start->getYaw());
}

std::shared_ptr<Arc> Arc::getArcFromStartEnd(Position* start, Position* end) {
	return getArcFromStartEndDistance(start, end, start->distanceTo(end));
}

Arc::~Arc() {

}

double Arc::getAlpha() {
	return alpha;
}

double Arc::getRadius() {
	return radius;
}

void Arc::setBackwards(bool backwards) {
	this->backwards = backwards;
}

bool Arc::isBackwards() {
	return backwards;
}

bool Arc::isCounterclockwiseForDrawing() {
	// da es kein XOR gibt, mit !=
	return counterclockwise != backwards;
}

rrt_msgs::Curve Arc::getMsg() {
	rrt_msgs::Curve curvemsg;
	curvemsg.type = "arc";
	curvemsg.x = reference.getX();
	curvemsg.y = reference.getY();

	// theta1 ist start, theta2 ist ende
	// die Winkel sollen für die nachrichten counterclockwise sortiert sein!
	if (isCounterclockwiseForDrawing()) {
		curvemsg.a = deg(theta1);
		curvemsg.b = deg(theta2);
	} else {
		curvemsg.a = deg(theta2);
		curvemsg.b = deg(theta1);
	}
	curvemsg.c = radius;
	return curvemsg;
}

double Arc::deg(double rad) {
	return fmod(360 + rad * 180 / M_PI, 360);
}

Position Arc::calculateEnd() {
	return reference.getPosOnCircle(theta2, radius, counterclockwise);
}

std::shared_ptr<Curve> Arc::getReverse() {
	return std::make_shared<Arc>(reference, radius, distance, Node::normAngle(yaw + M_PI),
			calculateEndYaw(), theta2, theta1, counterclockwise, !backwards);
}



double Arc::getalphafordrawing() {
	if (!backwards) {
		return alpha;
	} else if (alpha > 0) {	//&& backwards
		return alpha - 2 * M_PI;
	} else { // alpha < 0 && backwards
		return alpha + 2 * M_PI;
	}
}

std::vector<Position> Arc::calculatePointsOnCurve(double maxDistance) {
	std::vector<Position> points;
	int n = calculateCost() / maxDistance + 1;
	double stepsize = getalphafordrawing() / n; // vorzeichen ist damit bereits korrigiert!
	double theta = theta1;

	for (int i = 0; i < n; i++) {
		theta += stepsize;
		points.push_back(reference.getPosOnCircle(theta, radius, counterclockwise));
	}
	return points;
}

void Arc::calcAlphaAndRadius() {
	// TODO ! wenn länge=0 bzw. alpha=0 und radius-> inf den radius auf 1 fixieren
	// Winkel von radius nach start, beim ersten durchlauf ist radius 0 => richtung egal
	double radiusstartyaw = counterclockwise ? startyaw - M_PI / 2 : startyaw + M_PI / 2;

	Position start = reference.getPosWithDirectionDistanceYaw(radiusstartyaw, radius, startyaw);

	// alpha = 2* angledelta
	// echte Länge: alpha * r
	// r ist abhängig von distance, aus kosinussatz folgt: a^2 = 2r^2 - r^2 cos alpha
	// => r = distance / sqrt(2-cos(alpha))
	// => Länge = alpha * distance / ...
	alpha = 2 * Node::normAngle(yaw - startyaw); // => twist + alpha/2 = yaw, twist + alpha = endyaw

	// yaw-twist ist ma-pi bis pi => 2* ist -2pi bis 2pi
	if (distance > 0) {
		radius = distance / sqrt(2 - 2 * cos(alpha));
	} else {
		radius = 1;
	}

	counterclockwise = alpha > 0;

	radiusstartyaw = counterclockwise ? startyaw - M_PI / 2 : startyaw + M_PI / 2; // Winkel von radius nach start
	reference = start.getPosWithDirectionAndDistance(M_PI + radiusstartyaw, radius); // hier negierten Winkel, da wir zurückwollen
	theta1 = reference.angleTo(&start);
	theta2 = Node::normAngle(theta1 + alpha); //oder eingentlich angleTO ende
}

double Arc::calculateCost() {
	return fabs(getalphafordrawing()) * radius;
}

double Arc::calculateEndYaw() {
	return Node::normAngle(startyaw + alpha);
}

void Arc::setConstraints(double maxDistance, double minRadius) {
	if (distance > maxDistance) {
		distance = maxDistance;
		calcAlphaAndRadius();
	}

	// bei alpha=0 bzw. radius=inf ist reference auch inf => probleme
	if (std::isinf(radius) || radius > 1000 || alpha == 0) {
		yaw = startyaw + 0.01;
		calcAlphaAndRadius();
	}

	// Radius zu klein oder vorwärtskurve hat ziel das hinter uns liegt oder rückwärtskurve und Ziel vor uns
	if (radius < minRadius || (!backwards && fabs(alpha) > M_PI) || (backwards && fabs(alpha) < M_PI)) {
		double max_angle = getMaxAngleChange(distance, minRadius);
		double angledelta = (alpha > 0) ? max_angle : -max_angle; // winkel zwischen verbindungslinie und fahrzeugblickwinkel
		// => winkel des elternteil + maximum in vorherige richtung
		if (!backwards) {
			yaw = startyaw + angledelta;
		} else {
			yaw = startyaw + M_PI - angledelta;
		}
		calcAlphaAndRadius();
	}
	// bei Optiomalen Kreis komm raus: winkel fahrzeug = 2*Winkel verbindungslinie
	// da wir nur funktionierende haben, können wir es auf 2*Winkel setzen! (das sind dann aber nicht alle sondern nur der Optimale Winkel

	// wir fahren einen weg, nicht max oder min
	// Problem: man fährt nur kurven, d.h. bei einer geraden strecke ist man nur am schaukeln => Winkel reduzieren!
	// => rewiring + "echte" Kosten
}


Position Arc::getStart() {
	return reference.getPosOnCircle(theta1, radius, counterclockwise);
}
