#ifndef SRC_CURVES_LINE_H_
#define SRC_CURVES_LINE_H_

#include "Curve.h"

class Line: public Curve {
public:
	Line(Position start, double x, double y, double distance, double yaw);
	virtual ~Line();
	void setMaxDistance(double maxDistance);
	static std::shared_ptr<Line> getLineFromStartEndDistanceYaw(Position* start, Position* end, double distance, double yaw);
	static std::shared_ptr<Line> getLineFromStartEndDistance(Position* start, Position* end, double distance);
	static std::shared_ptr<Line> getLineFromStartEnd(Position* start, Position* end);
	rrt_msgs::Curve getMsg();
	Position getStart();
	Position calculateEnd();
	double calculateEndYaw();
	std::shared_ptr<Curve> getReverse();
	std::vector<Position> calculatePointsOnCurve(double maxDistance);
protected:
	void interpolateTo(double distance);
	void rotateTo(double yaw);

	double x = 0;
	double y = 0;
	bool reverse = false;

};

#endif /* SRC_CURVES_LINE_H_ */
