#ifndef SRC_CURVES_CURVE_H_
#define SRC_CURVES_CURVE_H_
#include "data/Position.h"
#include <rrt_msgs/Curve.h>

class Curve {
public:
	Curve(double distance, double yaw);
	virtual ~Curve();
	virtual Position getStart() = 0;
	virtual Position calculateEnd() = 0;
	virtual double calculateEndYaw();
	virtual double calculateCost();
	virtual std::shared_ptr<Curve> getReverse() = 0;
	/**
	 * Ohne Anfang, mit Ende, von vorne nach hinten aufgezählt, da wir für HybridDurbin die Endpunkte brauchen.Für Durbin muss auch so immer die Übergänge berechnet werden
	 * wir nehmen an das der Startpunkt immer bereits existiert von Kurven => die Enden zu prüfen entfernt theoretisch das benötigen von Knoten prüfen!
	 */
	virtual std::vector<Position> calculatePointsOnCurve(double maxDistance) = 0;
	double getDistance();
	virtual rrt_msgs::Curve getMsg() = 0;
protected:
	static double getXFromDistanceYaw(double distance, double yaw);
	static double getYFromDistanceYaw(double distance, double yaw);
	static double getYawFromXY(double y, double x);
	static double getDistanceFromXY(double x, double y);
	static double getMaxAngleChange(double distance, double turningRadius);

	double distance = 0;
	double yaw = 0;
	Position reference;
};

#endif /* SRC_CURVES_CURVE_H_ */
