#include "Curve.h"
#include <cmath>
#include "data/Node.h"

Curve::Curve(double distance, double yaw) {
	this->distance = distance;
	this->yaw = yaw;
}

Curve::~Curve() {

}

double Curve::getXFromDistanceYaw(double distance, double yaw) {
	return cos(yaw) * distance;
}

double Curve::getYFromDistanceYaw(double distance, double yaw) {
	return sin(yaw) * distance;
}

double Curve::getYawFromXY(double x, double y) {
	return atan2(y,x);
}

double Curve::getDistanceFromXY(double x, double y) {
	return hypot(x, y);
}

double Curve::calculateCost() {
	return getDistance();
}

double Curve::getDistance() {
	return distance;
}

double Curve::calculateEndYaw() {
	return yaw;
}

double Curve::getMaxAngleChange(double distance, double turningRadius) {
	// TODO evtl. für ein paar werte speichern um Performance zu verbessern
	// für zu große MAX_EDGE_LENGTH aufpassen!, evtl. beschränken
	return M_PI / 2 - acos(distance / (2 * turningRadius));
	// Kosinussatz, wendekreis^2 - wendekreis^2 = 0 oben im Bruch, danach einmal max. kantenlänge kürzen
}
