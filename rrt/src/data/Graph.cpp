#include "Graph.h"
#include <ros/ros.h>
#include <random>

Graph::Graph() {

}

Graph::~Graph() {

}

Node* Graph::getNearestNode(Node* currentNode) {
	return getNearestNodeWithDistance(currentNode).node;
}

std::vector<Node*> Graph::getPathTo(Node* target) {
	// erstellt Vector von target zu fixed
	std::vector<Node*> path = std::vector<Node*>();
	Node* currentNode = target;
	while (currentNode != nullptr) {
		path.push_back(currentNode);
		currentNode = currentNode->getPrev();
	}
	// reverse damit von fixed zu target
	std::reverse(path.begin(), path.end());
	return path;
}

Path* Graph::followPrevFrom(Node* target) {
	Path* path = new Path();
	for (Node* currentNode : getPathTo(target)) {
		// getPathTo ist bereits fixed zu target sortiert! => push_back auf path
		if (currentNode->getEdge() != nullptr) {
			std::list<Position> curvepoints;
			// die curves sind vom vorgänger zu uns sortiert => genauso wie fixed zu target
			for (std::shared_ptr<Curve>& curve : currentNode->getEdge()->getCurves()) {
				for (Position& pos : curve->calculatePointsOnCurve(MAX_PATH_EDGE_DISTANCE)) {
					// TODO Cost richtig berechnen
					path->push_back(pos, currentNode->getCost());
				}
			}
		} else {
			path->push_back(currentNode->getPosition(), currentNode->getCost());
		}
	}
	return path;
}

NodeWithDistance Graph::getCheapestPathTo(Node* target, double maxDistance) {
	std::vector<NodeWithDistance> neighbouringNodes = getNodesNear(target, maxDistance);
	if (neighbouringNodes.size() == 0) {
		return NodeWithDistance(nullptr, 0.0); // IDEA besser mit kein Weg gefunden umgehen?
	}
	NodeWithDistance cheapestNodeWithDistance = neighbouringNodes[0];
	for (NodeWithDistance& nodeWithDistance : neighbouringNodes) {
		if (nodeWithDistance.distance + nodeWithDistance.node->getCost()
				< cheapestNodeWithDistance.distance + cheapestNodeWithDistance.node->getCost()) {
			cheapestNodeWithDistance = nodeWithDistance;
		}
	}
	return cheapestNodeWithDistance;
}

rrt_msgs::Graph Graph::getGraphMsg() {
	rrt_msgs::Graph new_graph;

	for (Node*& node : getNodes()) {
		std::shared_ptr<Edge> edge = node->getEdge();
		if (edge != nullptr) {
			for (std::shared_ptr<Curve>& curve : edge->getCurves()) {
				new_graph.curves.push_back(curve->getMsg());
			}
		}
	}

	ROS_INFO("Anzahl Kurven: %lu", new_graph.curves.size());
	new_graph.header.frame_id = "map";
	new_graph.header.stamp = ros::Time::now();

	return new_graph;
}


Node* Graph::getNode(int index) {
	return getNodes()[index];
}
