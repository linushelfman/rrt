
class Node;

#ifndef SRC_NODE_H_
#define SRC_NODE_H_

#include <vector>
#include "Position.h"
#include "edges/Edge.h"

class Node : public Position {
public:
	Node(Position newPosition, Node* parent, double distance, newEdgeFunc newEdge);
	virtual ~Node();

	void setEdge(std::shared_ptr<Edge> edge);
	std::shared_ptr<Edge> getEdge();

	Node* getPrev();

	double getCost();

	void setOutgoingEdges(std::vector<std::shared_ptr<Edge>> outgoingEdges);
	std::vector<std::shared_ptr<Edge>> getOutgoingEdges();
	void addOutgoingEdge(std::shared_ptr<Edge> outgoingEdge);
	void removeOutgoingEdge(Node* otherNode);
	bool hasOutgoingEdges();
	bool skipCheck();
	void setSkipCheck();
private:
	std::shared_ptr<Edge> edge = nullptr;
	std::vector<std::shared_ptr<Edge>> outgoingEdges;
	bool hasoutgoingedges = false;
	bool skipcheck = false;
};


struct NodeWithDistance {
	NodeWithDistance(Node* node = nullptr, double distance = 0) {
		this->node = node;
		this->distance = distance;
	}
	Node* node;
	double distance;
};
#endif /* SRC_NODE_H_ */
