#include "VectorGraph.h"
#include <algorithm>

VectorGraph::VectorGraph() {

}

VectorGraph::~VectorGraph() {
	for (Node* node : nodes) {
		delete node;
	}
}

void VectorGraph::addNode(Node* newNode) {
	nodes.push_back(newNode);
}

void VectorGraph::deleteNode(Node* oldNode) {
	// TODO bessere Lösung finden
	nodes.erase(std::remove(nodes.begin(), nodes.end(), oldNode), nodes.end());
}

NodeWithDistance VectorGraph::getNearestNodeWithDistance(Position* currentNode) {
	NodeWithDistance nearestNodeWithDistance;
	nearestNodeWithDistance.distance = 1e10; // IDEA anders umsetzen als einfach nur eine große Zahl
	double squaredDistance;
	for (Node*& node : nodes) {
		if (node == currentNode)
			continue;
		squaredDistance = currentNode->squaredDistanceTo(node);
		if (squaredDistance < nearestNodeWithDistance.distance) {
			nearestNodeWithDistance.distance = squaredDistance;
			nearestNodeWithDistance.node = node;
		}
	}

	nearestNodeWithDistance.distance = currentNode->distanceTo(nearestNodeWithDistance.node);
	return nearestNodeWithDistance;
}

std::vector<NodeWithDistance> VectorGraph::getNodesNear(Position* currentNode, double maxDistance) {
	std::vector<NodeWithDistance> neighbouringNodes;
	double distance;
	for (Node*& node : nodes) {
		if (node == currentNode)
			continue;
		distance = currentNode->distanceTo(node);
		if (distance < maxDistance) {
			neighbouringNodes.push_back(NodeWithDistance(node, distance));
		}
	}

	return neighbouringNodes;
}

int VectorGraph::size() {
	return nodes.size();
}

std::vector<Node*> VectorGraph::getNodes() {
	return nodes;
}

NearbyData VectorGraph::getNearbyData(Position* currentNode, double maxDistance, double maxAncestorDistance) {
	// TODO umbauen auf squareddistance?
	NearbyData nearbyData;
	nearbyData.nearestNode.distance = 1e10;
	double distance;
	for (Node*& node : nodes) {
		if (node == currentNode)
			continue;
		distance = currentNode->distanceTo(node);
		if (distance < nearbyData.nearestNode.distance) {
			nearbyData.nearestNode.distance = distance;
			nearbyData.nearestNode.node = node;
		}
		if (distance < maxAncestorDistance) {
			nearbyData.nodesInAncestorDistance.push_back(NodeWithDistance(node, distance));
			if (distance < maxDistance) {
				nearbyData.nodesInDistance.push_back(NodeWithDistance(node, distance));
			}
		}
	}

	return nearbyData;
}

Node* VectorGraph::getNode(int index) {
	return nodes[index];
}

graphGen VectorGraph::getGraphGen() {
	return []() {return new VectorGraph();};
}
