#include "BoxGraph.h"

#include <algorithm>
#include <cmath>
#include <ros/ros.h>

BoxGraph::BoxGraph(double spacing) {
	SPACING = spacing;
}

BoxGraph::~BoxGraph() {
	for (std::pair<indexType, blockType> blockentry : mapOfBlocks) {
		for (Node*& node : blockentry.second) {
			delete node;
		}
	}
}

void BoxGraph::addNode(Node* newNode) {
	nodes_size++;
	indexType index = indexOfNode(newNode);
	if (!indexExists(index)) {
		mapOfBlocks[index] = blockType();
	}
	mapOfBlocks[index].push_back(newNode);
}

void BoxGraph::deleteNode(Node* oldNode) {
	nodes_size--;
	auto container = mapOfBlocks[indexOfNode(oldNode)];
	// IDEA eigentlich nur wenn wirklich was gelöscht wurde
	// TODO Container mit besserer Löschfunktion nutzen!
	// unordered_set?
	container.erase(std::remove(container.begin(), container.end(), oldNode), container.end());
}

void BoxGraph::addIfExists(std::vector<indexType>& indexes, const indexType& index) {
	if (indexExists(index)) {
		indexes.push_back(index);
	}
}

std::vector<indexType> BoxGraph::getIndexesOnSquareRingWithRadius(Position* currentNode, int radius) {
	std::vector<indexType> indexes;
	indexType nodeIndex = indexOfNode(currentNode);
	// bei radius 0 nur den aktuellen Knoten
	if (radius == 0) {
		addIfExists(indexes, nodeIndex);
	} else {
		// im Uhrzeigersinn, gleichzeitig von allen Ecken starten und die Felder prüfen
		// => die Ecken nicht doppelt prüfen, bei r ist die Kantenlänge 2*r+1, wenn man ecke nicht doppelt prüft folgt 2*R
		// bei y zeigt nach oben und x nach rechts:
		int right = nodeIndex.first + radius;
		int left = nodeIndex.first - radius;
		int top = nodeIndex.second + radius;
		int bottom = nodeIndex.second - radius;
		for (int i= 0; i < 2*radius; i++) {
			addIfExists(indexes, indexType(right, top - i)); // rechts, von oben nach unten zählen
			addIfExists(indexes, indexType(right - i, bottom)); // unten, von rechts nach links zählen
			addIfExists(indexes, indexType(left, bottom + i)); // links, von unten nach oben zählen
			addIfExists(indexes, indexType(left + i, top)); // oben, von links nach rechts zählen
		}
	}
	return indexes;
}

NodeWithDistance BoxGraph::getNearestNodeWithDistance(Position* currentNode) {
	int radius = 0;
	NodeWithDistance nearestNode;
	nearestNode.distance = 1e10;

	while (nearestNode.node == nullptr) {
		for (indexType& index : getIndexesOnSquareRingWithRadius(currentNode, radius)) {
			for (Node*& node : mapOfBlocks[index]) {
				if (node == currentNode)
					continue;
				double squaredDistance = currentNode->squaredDistanceTo(node);
				if (squaredDistance < nearestNode.distance) {
					nearestNode.node = node;
					nearestNode.distance = squaredDistance;
				}
			}
		}
		radius++;
	}
	radius--;
	// prüfen ob es noch Blöcke gibt die nähere Knoten enthalten können
	for (indexType index : getBoxesWithRadiusLessOrEqual(currentNode, sqrt(nearestNode.distance), radius)) {
		for (Node*& node : mapOfBlocks[index]) {
			if (node == currentNode)
				continue;
			double squaredDistance = currentNode->squaredDistanceTo(node);
			if (squaredDistance < nearestNode.distance) {
				nearestNode.node = node;
				nearestNode.distance = squaredDistance;
			}
		}
	}

	nearestNode.distance = sqrt(nearestNode.distance);
	// Wir gehen davon aus das immer min. ein Knoten existiert
	return nearestNode;
}

graphGen BoxGraph::getGraphGen(double spacing) {
	return [spacing]() {return new BoxGraph(spacing);};
}

bool BoxGraph::indexExists(const indexType& index) {
	return mapOfBlocks.find(index) != mapOfBlocks.end();
}

std::vector<indexType> BoxGraph::getBoxesWithRadiusLessOrEqual(Position* node, double radius, int minradius) {
	// minradius = -1 bedeutet alle, 0 heiß ohne nodeIndex, ...
	std::vector<indexType> indexes;
	indexType nodeIndex = indexOfNode(node);
	double squaredRadius = radius*radius;
	for (int x = indexOfDouble(node->getX() - radius); x <= indexOfDouble(node->getX() + radius); x++) {
		for (int y = indexOfDouble(node->getY() - radius);
				y <= indexOfDouble(node->getY() + radius); y++) {
			indexType index = indexType(x, y);
			// Feld existiert
			// Feld ist nicht im minradius (perf optimierung für nearestNode in getNearbyData)
			if (indexExists(index) && abs(nodeIndex.first - x) > minradius
					&& abs(nodeIndex.second - y) > minradius) {
				double minXDiff = std::min<double>(abs(x * SPACING - node->getX()),
						abs((x + 1) * SPACING - node->getX()));
				double minYDiff = std::min<double>(abs(y * SPACING - node->getY()),
						abs((y + 1) * SPACING - node->getY()));

				// aktuellen Quadraten immer prüfen
				if ((nodeIndex.first == x && nodeIndex.second == y)
						// y-verschobene Quadranten nur y-diff prüfen
						|| (nodeIndex.first == x && minYDiff < radius)
						// x-verschobene Quadranten nur x-diff prüfen
						|| (nodeIndex.second == y && minXDiff < radius)
						// sonst den Abstand zu nähester Ecke prüfen
						|| minXDiff * minXDiff + minYDiff * minYDiff < squaredRadius) {
					indexes.push_back(index);
				}
			}
		}
	}
	return indexes;
}

std::vector<NodeWithDistance> BoxGraph::getNodesNear(Position* currentNode, double maxDistance) {
	std::vector<NodeWithDistance> neighbouringNodes;
	double squaredDistance;
	double maxDistanceSquared = maxDistance * maxDistance;
	for (indexType& index : getBoxesWithRadiusLessOrEqual(currentNode, maxDistance)) {
		for (Node* node : mapOfBlocks[index]) {
			if (node == currentNode) {
				continue;
			}
			squaredDistance = currentNode->squaredDistanceTo(node);
			if (squaredDistance < maxDistanceSquared) {
				neighbouringNodes.push_back(NodeWithDistance(node, sqrt(squaredDistance)));
			}
		}
	}
	return neighbouringNodes;
}

int BoxGraph::size() {
	return nodes_size;
}

std::vector<Node*> BoxGraph::getNodes() {
	std::vector<Node*> allNodes;
	for (std::pair<indexType, blockType> block : mapOfBlocks) {
		allNodes.insert(allNodes.end(), block.second.begin(), block.second.end());
	}
	return allNodes;
}

indexType BoxGraph::indexOfNode(Position* node) {
	return indexType(indexOfDouble(node->getX()), indexOfDouble(node->getY()));
}

int BoxGraph::indexOfDouble(double xy) {
	// floor, damit negative zahlen auch nach unten gerundet werden
	return floor(xy / SPACING);
}

NearbyData BoxGraph::getNearbyData(Position* currentNode, double maxDistance, double maxAncestorDistance) {
	// TODO umbauen auf squareddistance?
	NearbyData nearbyData;
	nearbyData.nearestNode.distance = 1e10;
	double distance;
	for (indexType index : getBoxesWithRadiusLessOrEqual(currentNode, maxAncestorDistance)) {
		for (Node*& node : mapOfBlocks[index]) {
			if (node == currentNode)
				continue;
			distance = currentNode->distanceTo(node);
			if (distance < nearbyData.nearestNode.distance) {
				nearbyData.nearestNode.distance = distance;
				nearbyData.nearestNode.node = node;
			}
			if (distance < maxAncestorDistance) {
				nearbyData.nodesInAncestorDistance.push_back(NodeWithDistance(node, distance));
				if (distance < maxDistance) {
					nearbyData.nodesInDistance.push_back(NodeWithDistance(node, distance));
				}
			}
		}
	}

	// wir haben keinen Knoten im gegebenen Radius gefunden
	if (nearbyData.nearestNode.node == nullptr) {
		// wir suchen also nur den nächsten Knoten, das wir Gebietet doppelt absuchen ist egal,
		// da wir nur ein paar leere vektoren doppelt durchsuchen
		nearbyData.nearestNode = getNearestNodeWithDistance(currentNode);
	}

	return nearbyData;
}
