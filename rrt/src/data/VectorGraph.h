#ifndef SRC_VECTORGRAPH_H_
#define SRC_VECTORGRAPH_H_

#include "Graph.h"

class VectorGraph: public Graph {
public:
	VectorGraph();
	~VectorGraph();

	void addNode(Node* newNode);
	void deleteNode(Node* oldNode);
	NodeWithDistance getNearestNodeWithDistance(Position* currentNode);
	int size();

	std::vector<NodeWithDistance> getNodesNear(Position* currentNode, double maxDistance);
	NearbyData getNearbyData(Position* currentNode, double maxDistance, double maxAncestorDistance);
	std::vector<Node*> getNodes();
	Node* getNode(int index);

	static graphGen getGraphGen();
private:
	std::vector<Node*> nodes;
};

#endif /* SRC_VECTORGRAPH_H_ */
