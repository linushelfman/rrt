#include "Path.h"
#include <ros/ros.h>

Path::Path() {
}

Path::~Path() {
	// die Knoten werden vom Graph gelöscht
}

void Path::push_back(Position position, double cost) {
	nodes.push_back(PositionWithCost(position,cost));
}

void Path::push_front(Position position, double cost) {
	nodes.push_front(PositionWithCost(position,cost));
}

int Path::size() {
	return nodes.size();
}

fub_trajectory_msgs::Trajectory Path::toTrajectoryMsg() {
	fub_trajectory_msgs::Trajectory trajectory_path;
	double duration = 0;
	for (PositionWithCost& node : nodes) {
		fub_trajectory_msgs::TrajectoryPoint next_point;

		next_point.pose.position = node.position.getPositionMsg();
		next_point.pose.orientation = node.position.getOrientationMsg();

		// IDEA diese werte auch setzen?
		ros::Duration dur(duration++);
		next_point.time_from_start = dur; // IDEA aus Cost berechnen!
		//next_point.velocity = 1;
		trajectory_path.trajectory.push_back(next_point);

		ROS_DEBUG("Knotenkosten: %.3f", node.cost);
	}

	ROS_INFO("Pfadlange: %u", size());
	ROS_INFO("Pfadkosten: %.3f", nodes.back().cost);
	trajectory_path.header.frame_id = "map";
	trajectory_path.header.stamp = ros::Time::now();
	trajectory_path.child_frame_id = "base_link";

	return trajectory_path;
}

nav_msgs::Path Path::toPathMsg() {
	nav_msgs::Path trajectory_path;
	for (PositionWithCost& node : nodes) {
		geometry_msgs::PoseStamped next_point;

		next_point.pose.position = node.position.getPositionMsg();
		next_point.pose.orientation = node.position.getOrientationMsg();

		trajectory_path.poses.push_back(next_point);
	}

	trajectory_path.header.frame_id = "odom";
	trajectory_path.header.stamp = ros::Time::now();

	return trajectory_path;
}

void Path::reverse() {
	// gesamtlänge
	double maxcost = nodes.back().cost;
	// Reihenfolge umdrehen
	nodes.reverse();
	// & damit änderungen angewendet werden
	for (PositionWithCost& node : nodes) {
		// Knoten (Winkel umdrehen)
		// node.position.reverse();
		// Kosten umdrehen
		node.cost = maxcost - node.cost;
	}
}
