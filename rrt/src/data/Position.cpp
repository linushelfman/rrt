#include "Position.h"
#include <tf/tf.h>
#include <iomanip>

Position::Position(double x, double y, double yaw) {
	setPosition(x, y, yaw);
}


Position::~Position() {
	// hat keine dynamischen Daten
}

void Position::setPosition(double x, double y, double yaw) {
	this->x = x;
	this->y = y;
	this->yaw = yaw;
}

geometry_msgs::Point Position::getPositionMsg() {
	geometry_msgs::Point point;
	point.x = x;
	point.y = y;
	point.z = 0;
	return point;
}

Position Position::getPosition() {
	return Position(x,y,yaw);
}

double Position::distanceTo(Position* otherNode) {
	return std::sqrt((otherNode->getX() - x) * (otherNode->getX() - x)
			+ (otherNode->getY() - y) * (otherNode->getY() - y));
}

double Position::squaredDistanceTo(Position* otherNode) {
	return (otherNode->getX() - x) * (otherNode->getX() - x)
			+ (otherNode->getY() - y) * (otherNode->getY() - y);
}

geometry_msgs::Quaternion Position::getOrientationMsg() {
	return tf::createQuaternionMsgFromYaw(yaw);
}

// IDEA inline definitionen?
double Position::getX() {
	return x;
}

double Position::getY() {
	return y;
}

double Position::getYaw() {
	return yaw;
}

void Position::setYaw(double yaw) {
	this->yaw = yaw;
}

double Position::getAngle(Position* a, Position* b) {
	return atan2(b->getY() - a->getY(), b->getX() - a->getX());
}

double Position::normAngle(double angle) {
	return angle - 2 * M_PI * floor((angle + M_PI) / (2 * M_PI));
}

std::ostream& operator<<(std::ostream& os, const Position& p) {
	os << std::fixed << std::setprecision(3) << p.x << " " << p.y << " " << p.yaw;
	return os;
}

void Position::setPosition(Position pos) {
	setPosition(pos.getX(),pos.getY(),pos.getYaw());
}

void Position::reverse() {
	yaw = normAngle(yaw + M_PI);
}

double Position::equal(Position* a, Position* b) {
	static double nearnull = 0.0001;
	return fabs(a->getX() - b->getX()) < nearnull
			&& fabs(a->getY() - b->getY()) < nearnull
			&& fabs(normAngle(a->getYaw() - b->getYaw())) < nearnull;
}

double Position::angleTo(Position* otherNode) {
	return getAngle(this, otherNode);
}

Position Position::getPosOnCircle(double alpha, double radius, double counterclockwise) {
	if (counterclockwise) {
		return getPosOnLCircle(alpha, radius);
	} else {
		return getPosOnRCircle(alpha, radius);
	}
}

Position Position::getPosOnLCircle(double alpha, double radius) {
	return getPosWithDirectionDistanceYaw(alpha, radius, normAngle(alpha + M_PI / 2));
}

Position Position::getPosOnRCircle(double alpha, double radius) {
	return getPosWithDirectionDistanceYaw(alpha, radius, normAngle(alpha - M_PI / 2));
}

Position Position::getPosWithRelDirectionAndDistance(double yaw, double distance) {
	return getPosWithDirectionAndDistance(this->yaw + yaw, distance);
}

Position Position::getPosWithDirectionAndDistance(double yaw, double distance) {
	return getPosWithDirectionDistanceYaw(yaw, distance, yaw);
}

Position Position::getPosWithDirectionDistanceYaw(double alpha, double distance, double newyaw) {
	return Position(x + cos(alpha) * distance, y + sin(alpha) * distance, newyaw);
}

double Position::angleEqual(double anglea, double angleb) {
	return fabs(normAngle(anglea - angleb)) < 0.01;
}
