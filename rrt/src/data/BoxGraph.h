#ifndef SRC_BOXGRAPH2_H_
#define SRC_BOXGRAPH2_H_

#include "Graph.h"
#include <map>
#include <set>

typedef std::pair<int,int> indexType;
typedef std::vector<Node*> blockType;

/**
 * Teil das Feld in ein Gitter von x*y Feldern auf
 * per Map werden nur benötigte Felder gespeichert
 */
class BoxGraph: public Graph {
public:
	BoxGraph(double spacing);
	virtual ~BoxGraph();
	void addNode(Node* newNode);
	void deleteNode(Node* oldNode);
	NodeWithDistance getNearestNodeWithDistance(Position* currentNode);
	int size();
	std::vector<NodeWithDistance> getNodesNear(Position* currentNode, double maxDistance);
	std::vector<Node*> getNodes();
	NearbyData getNearbyData(Position* currentNode, double maxDistance, double maxAncestorDistance);

	static graphGen getGraphGen(double spacing = 1.0);
private:
	// Hilfsfunktionen
	indexType indexOfNode(Position* node);
	int indexOfDouble(double xy);

	std::vector<indexType> getBoxesWithRadiusLessOrEqual(Position* node, double radius, int minradius = -1);
	std::vector<indexType> getIndexesOnSquareRingWithRadius(Position* currentNode, int radius);
	bool indexExists(const indexType& index);
	void addIfExists(std::vector<indexType>& indexes, const indexType& index);

	int nodes_size = 0;
	std::map<indexType, blockType> mapOfBlocks;

	double SPACING = 1.0;
};

#endif /* SRC_BOXGRAPH2_H_ */
