#ifndef SRC_PATH_H_
#define SRC_PATH_H_

#include <list>
#include "Position.h"
#include <iterator>
#include <fub_trajectory_msgs/Trajectory.h>
#include <nav_msgs/Path.h>



/**
 * Beschreibt einen Pfad aus Knoten
 */
class Path {
public:
	Path();
	virtual ~Path();
	void push_back(Position position, double cost);
	void push_front(Position position, double cost);
	fub_trajectory_msgs::Trajectory toTrajectoryMsg();
	nav_msgs::Path toPathMsg();
	int size();
	void reverse();

private:
	struct PositionWithCost {
		PositionWithCost(Position position, double cost) {
			this->position = position;
			this->cost = cost;
		}
		Position position;
		double cost;
	};

	std::list<PositionWithCost> nodes;
	/**
	 * Alternativen: deque? ist evtl. schneller, man kann den Pfad dann aber nicht in der Mitte so schnell anpassen
	 * Der Path soll die implementierung verstecken!!!
	 * wie kann man gut zugreifen? iteratoren sind doof/nicht generisch genug, eventuel mit Typ T arbeiten und dann vom erzeuger bestimmen lassen?
	 */
};

#endif /* SRC_PATH_H_ */
