#ifndef SRC_DATA_POSITION_H_
#define SRC_DATA_POSITION_H_

#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Point.h>
#include <ostream>

class Position {
public:
	Position(double x=0, double y=0, double yaw=0);
	virtual ~Position();

	void setPosition(double x, double y, double yaw);
	void setPosition(Position pos);
	Position getPosition();
	Position getPosWithRelDirectionAndDistance(double yaw, double distance);
	Position getPosWithDirectionAndDistance(double yaw, double distance);
	Position getPosWithDirectionDistanceYaw(double alpha, double distance, double yaw);
	Position getPosOnCircle(double alpha, double radius, double counterclockwise);
	/**
	 * Links = Mathematischer drehsinn bzw. gegen Uhrzeiger
	 */
	Position getPosOnLCircle(double alpha, double radius);
	/**
	 * Rechts = Uhrzeigersinn bzw. gegen Mathematischer drehsinn
	 */
	Position getPosOnRCircle(double alpha, double radius);
	double getX();
	double getY();
	double getYaw();
	void setYaw(double yaw);
	friend std::ostream& operator<<(std::ostream& os, const Position& p);
	void reverse();

	double distanceTo(Position* otherNode);
	double squaredDistanceTo(Position* otherNode);
	double angleTo(Position* otherNode);

	// TODO evtl. extraklasse die nur Daten umwandelt?
	geometry_msgs::Quaternion getOrientationMsg();
	geometry_msgs::Point getPositionMsg();

	static double getAngle(Position* a, Position* b);
	static double normAngle(double angle);
	static double angleEqual(double anglea, double angleb);
	static double equal(Position* a, Position* b);
protected:
	double x = 0;
	double y = 0;
	// pos. yaw ist links
	double yaw = 0;
};

#endif /* SRC_DATA_POSITION_H_ */
