#include "Node.h"
#include <cmath>
#include <ros/ros.h>

Node::Node(Position newPosition, Node* parent, double distance, newEdgeFunc newEdge) :
		Position(newPosition.getX(), newPosition.getY(), newPosition.getYaw()) {
	setEdge(newEdge(parent, this, distance));
}

Node::~Node() {
	// TODO ! BestAcnestorproblem im free vom shared_ptr der edge...
	// so funktioniert es wenn alles gelöscht wird, wenn aber man nur einzelne Knoten löschen will, geht es nicht
//	for (std::shared_ptr<Edge>& outgoingEdge : outgoingEdges) {
//		outgoingEdge->getEnd()->removeOutgoingEdge(this);
//	}
}

Node* Node::getPrev() {
	if (edge != nullptr) {
		return edge->getStart();
	}
	return nullptr;
}

double Node::getCost() {
	if (edge == nullptr) {
		return 0;
	}
	return edge->getCostOfEnd();
}

void Node::setOutgoingEdges(std::vector<std::shared_ptr<Edge>> outgoingEdges) {
	this->outgoingEdges = outgoingEdges;
	for (std::shared_ptr<Edge>& outgoingEdge: outgoingEdges) {
		std::shared_ptr<Edge> reverseEdge = outgoingEdge->getReverse();
		reverseEdge->getStart()->addOutgoingEdge(reverseEdge);
		if (reverseEdge->getStart() == edge->getStart()) {
			setEdge(reverseEdge); // doppelte Kanten sparen
		}
	}
	hasoutgoingedges = true;
}

std::vector<std::shared_ptr<Edge>> Node::getOutgoingEdges() {
	// TODO in set umwandeln?
	return outgoingEdges; // Segfault beim kopieren hiervon, da wir keinen shared_ptr kopieren konnten?
}

void Node::addOutgoingEdge(std::shared_ptr<Edge> outgoingEdge) {
	outgoingEdges.push_back(outgoingEdge);
}

void Node::removeOutgoingEdge(Node* otherNode) {
	// TODO ! Das hier liefert bei BestAncestor fehler?
	outgoingEdges.erase(
			std::remove_if(outgoingEdges.begin(), outgoingEdges.end(), [otherNode](std::shared_ptr<Edge> outgoingEdge) {
				if (outgoingEdge->getEnd() == otherNode) {
					return true;
				}
				return false;}), outgoingEdges.end());
}

std::shared_ptr<Edge> Node::getEdge() {
	return edge;
}

void Node::setEdge(std::shared_ptr<Edge> newEdge) {
	edge = newEdge;

	for (std::shared_ptr<Edge>& outgoingEdge : outgoingEdges) {
		outgoingEdge->resetStartCost();
	}
}

bool Node::hasOutgoingEdges() {
	return hasoutgoingedges;
}

bool Node::skipCheck() {
	return skipcheck;
}

void Node::setSkipCheck() {
	skipcheck = true;
}
