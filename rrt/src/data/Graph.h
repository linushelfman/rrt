#ifndef SRC_GRAPH_H_
#define SRC_GRAPH_H_

#include "Path.h"
#include "Node.h"
#include <vector>
#include <utility>
#include "rrt_msgs/Graph.h"
#include <random>
#include <functional>

struct NearbyData {
	NodeWithDistance nearestNode;
	std::vector<NodeWithDistance> nodesInDistance;
	std::vector<NodeWithDistance> nodesInAncestorDistance;
};

/**
 * Beschreibt einen Graphen der alle Knoten enthält
 * Unterklassen für verschiedene Ansätze/Implementierungen
 */
class Graph {
public:
	Graph();
	virtual ~Graph();

	virtual void addNode(Node* newNode) = 0;
	virtual void deleteNode(Node* oldNode) = 0;
	Node* getNearestNode(Node* node);
	virtual NodeWithDistance getNearestNodeWithDistance(Position* currentNode) = 0;
	virtual int size() = 0;
	Path* followPrevFrom(Node* target);
	std::vector<Node*> getPathTo(Node* target);
	virtual std::vector<NodeWithDistance> getNodesNear(Position* currentNode, double maxDistance) = 0;
	virtual NearbyData getNearbyData(Position* currentNode, double maxDistance, double maxAncestorDistance) = 0;
	virtual Node* getNode(int index);
	NodeWithDistance getCheapestPathTo(Node* target, double maxDistance);
	virtual std::vector<Node*> getNodes() = 0;
	rrt_msgs::Graph getGraphMsg();
private:
	double MAX_PATH_EDGE_DISTANCE = 0.05;
};

typedef std::function<Graph*()> graphGen;

#endif /* SRC_GRAPH_H_ */
