#ifndef SRC_WRAPPERLIB_H_
#define SRC_WRAPPERLIB_H_

#include "rrt/Planner.h"

int parseLine(char *line);
int GetProcessMemory();
std::vector<std::shared_ptr<Planner>> getPlanners();
void markTime();
double timeSinceLastMark();

#endif /* SRC_WRAPPERLIB_H_ */
