### Quellcode für die Bachelorarbeit

Dies sind zwei ROS-Packages die für die [Modelautos der FU](https://github.com/AutoModelCar/AutoModelCarWiki/wiki) bzw. den [Seat Car Simulator](https://gitlab.iri.upc.edu/seat_adc/seat_car_simulator) gedacht sind.
Sie implementieren einen RRT mit ein paar Erweiterungen (RRT\*Smart, Informed RRT\*, ...).
Zum bauen wird das fub_trajectory_msgs-Package benötigt

### Benutzung

``` roslaunch seat_car_gazebo sim.launch spawn_circuit:=false ```

mit spawn_circuit kann man entscheiden ob die Karte angezeigt werden soll

``` roslaunch fub_controller ControllerModelCar.launch ```

führt den Controller aus, der das Auto abhängig vom Pfad steuert

``` rosrun rrt follow_path ```

führt alternativen/primitiven Follower aus

``` rosrun rrt rrt_planer 2 ```

startet den RRT

``` rosrun rrt rrt_wrapper```
``` rostopic pub -1 /rrt/cmd std_msgs/String "data: 'plan'" ```

startet den RRT-Wrapper

mit Befehl 'stop' stopt das Auto, mit 'plan' fährt es.
Das ziel kann man in RVIZ mit 2D Nav Goal angeben

``` rosrun rrt draw_map.py ```

startet die Karte
